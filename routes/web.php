<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', 'HomeController@index')->name('home');

Route::get('/login', function(){
	return view('auth.login'); 
});

Route::post('/in', 'Auth\LoginController@login')->name('in');

Route::post('/logout', 'Auth\LoginController@logout')->name('logout'); 
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@profile');
Route::post('/update', 'HomeController@profile')->name('update');
Route::post('/imageprofile', 'ProfileController@store')->name('image_profile');
Route::get('/membership', 'ProfileController@memberships')->name('membership');
Route::post('/profileimage/{id}', 'ProfileController@userfoto')->name('savephoto');

Route::get('/single', 'HomeController@single')->name('single');
Route::get('/ours-members', 'ProfileController@members')->name('ourmembers');
Route::get('payment/{type?}', 'PaymentController@pay')->name('payment');
Route::post('/api/payment', 'PaymentController@choose')->name('routeApi');
Route::get('profile/{id}', 'ProfileController@get_profile')->name('get_profile');
Route::post('/api/v1/friend', 'FriendController@addfriend');

Route::get('/testimonies', 'PostController@testimonios')->name('testimonies');
Route::get('/testimonies/{id}', 'PostController@single_testimonio')->name('single-testimonie');

Route::get('/events', 'PostController@eventos')->name('events');
Route::get('/events/{id}', 'PostController@single_evento')->name('single-event');

Route::get('/medellin', 'PostController@medellins')->name('medellins');
Route::get('/medellin/{id}', 'PostController@single_medellin')->name('single-medellin');

Route::get('/colombia', 'PostController@colombias')->name('colombias');
Route::get('/colombia/{id}', 'PostController@single_colombia')->name('single-colombia');

Route::get('/haslove', 'PostController@hasloves')->name('hasloves');
Route::get('/haslove/{id}', 'PostController@single_haslove')->name('single-haslove');



Route::get('/administrador', 'AdminController@index')->name('adminhome');
Route::get('administrador/usuarios', 'AdminController@users')->name('adminusers');
Route::get('administrador/profile/{id}', 'AdminController@details_user')->name('adminprofile');
Route::get('administrador/add', 'AdminController@add_user')->name('adminadd');
Route::post('administrador/store', 'AdminController@store_user')->name('adminstore');
Route::resource('administrador/posts', 'PostController');
Route::get('administrador/memberships', 'AdminController@memberships')->name('adminmemberships');
Route::get('administrador/memberships/edit', 'AdminController@edit_membership')->name('admineditmembership');


