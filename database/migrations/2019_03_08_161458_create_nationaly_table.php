<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNationalyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nationaly', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });

        DB::table('nationaly')->insert(['nombre'=> 'Afghanistan']);
        DB::table('nationaly')->insert(['nombre'=> 'Albania']);
        DB::table('nationaly')->insert(['nombre'=> 'Algeria']);
        DB::table('nationaly')->insert(['nombre'=> 'Andorra']);
        DB::table('nationaly')->insert(['nombre'=> 'Angola']);
        DB::table('nationaly')->insert(['nombre'=> 'Antigua and Barbuda']);
        DB::table('nationaly')->insert(['nombre'=> 'Argentina']);
        DB::table('nationaly')->insert(['nombre'=> 'Armenia']);
        DB::table('nationaly')->insert(['nombre'=> 'Australia']);
        DB::table('nationaly')->insert(['nombre'=> 'Austria']);
        DB::table('nationaly')->insert(['nombre'=> 'Azerbaijan']);
        DB::table('nationaly')->insert(['nombre'=> 'Bahamas']);
        DB::table('nationaly')->insert(['nombre'=> 'Bahrain']);
        DB::table('nationaly')->insert(['nombre'=> 'Bangladesh']);
        DB::table('nationaly')->insert(['nombre'=> 'Barbados']);
        DB::table('nationaly')->insert(['nombre'=> 'Belarus']);
        DB::table('nationaly')->insert(['nombre'=> 'Belgium']);
        DB::table('nationaly')->insert(['nombre'=> 'Belize']);
        DB::table('nationaly')->insert(['nombre'=> 'Benin']);
        DB::table('nationaly')->insert(['nombre'=> 'Bhutan']);
        DB::table('nationaly')->insert(['nombre'=> 'Bolivia']);
        DB::table('nationaly')->insert(['nombre'=> 'Bosnia and Herzegovina']);
        DB::table('nationaly')->insert(['nombre'=> 'Botswana']);
        DB::table('nationaly')->insert(['nombre'=> 'Brazil']);
        DB::table('nationaly')->insert(['nombre'=> 'Brunei']);
        DB::table('nationaly')->insert(['nombre'=> 'Bulgaria']);
        DB::table('nationaly')->insert(['nombre'=> 'Burkina Faso']);
        DB::table('nationaly')->insert(['nombre'=> 'Burundi']);
        DB::table('nationaly')->insert(['nombre'=> 'Cabo Verde']);
        DB::table('nationaly')->insert(['nombre'=> 'Cambodia']);
        DB::table('nationaly')->insert(['nombre'=> 'Cameroon']);
        DB::table('nationaly')->insert(['nombre'=> 'Canada']);
        DB::table('nationaly')->insert(['nombre'=> 'Central African Republic (CAR)']);
        DB::table('nationaly')->insert(['nombre'=> 'Chad']);
        DB::table('nationaly')->insert(['nombre'=> 'Chile']);
        DB::table('nationaly')->insert(['nombre'=> 'China']);
        DB::table('nationaly')->insert(['nombre'=> 'Colombia']);
        DB::table('nationaly')->insert(['nombre'=> 'Comoros']);
        DB::table('nationaly')->insert(['nombre'=> 'Democratic Republic of the Congo']);
        DB::table('nationaly')->insert(['nombre'=> 'Republic of the Congo']);
        DB::table('nationaly')->insert(['nombre'=> 'Costa Rica']);
        DB::table('nationaly')->insert(['nombre'=> 'Cote dIvoire']);
        DB::table('nationaly')->insert(['nombre'=> 'Croatia']);
        DB::table('nationaly')->insert(['nombre'=> 'Cuba']);
        DB::table('nationaly')->insert(['nombre'=> 'Cyprus']);
        DB::table('nationaly')->insert(['nombre'=> 'Czech Republic']);
        DB::table('nationaly')->insert(['nombre'=> 'Denmark']);
        DB::table('nationaly')->insert(['nombre'=> 'Djibouti']);
        DB::table('nationaly')->insert(['nombre'=> 'Dominica']);
        DB::table('nationaly')->insert(['nombre'=> 'Dominican Republic']);
        DB::table('nationaly')->insert(['nombre'=> 'Fiji']);
        DB::table('nationaly')->insert(['nombre'=> 'Finland']);
        DB::table('nationaly')->insert(['nombre'=> 'France']);
        DB::table('nationaly')->insert(['nombre'=> 'Gabon']);
        DB::table('nationaly')->insert(['nombre'=> 'Gambia']);
        DB::table('nationaly')->insert(['nombre'=> 'Georgia']);
        DB::table('nationaly')->insert(['nombre'=> 'Germany']);
        DB::table('nationaly')->insert(['nombre'=> 'Ghana']);
        DB::table('nationaly')->insert(['nombre'=> 'Greece']);
        DB::table('nationaly')->insert(['nombre'=> 'Grenada']);
        DB::table('nationaly')->insert(['nombre'=> 'Guatemala']);
        DB::table('nationaly')->insert(['nombre'=> 'Guinea']);
        DB::table('nationaly')->insert(['nombre'=> 'Guinea-Bissau']);
        DB::table('nationaly')->insert(['nombre'=> 'Guyana']);
        DB::table('nationaly')->insert(['nombre'=> 'Haiti']);
        DB::table('nationaly')->insert(['nombre'=> 'Honduras']);
        DB::table('nationaly')->insert(['nombre'=> 'Hungary']);
        DB::table('nationaly')->insert(['nombre'=> 'Iceland']);
        DB::table('nationaly')->insert(['nombre'=> 'India']);
        DB::table('nationaly')->insert(['nombre'=> 'Indonesia']);
        DB::table('nationaly')->insert(['nombre'=> 'Iran']);
        DB::table('nationaly')->insert(['nombre'=> 'Iraq']);
        DB::table('nationaly')->insert(['nombre'=> 'Ireland']);
        DB::table('nationaly')->insert(['nombre'=> 'Israel']);
        DB::table('nationaly')->insert(['nombre'=> 'Italy']);
        DB::table('nationaly')->insert(['nombre'=> 'Jamaica']);
        DB::table('nationaly')->insert(['nombre'=> 'Japan']);
        DB::table('nationaly')->insert(['nombre'=> 'Jordan']);
        DB::table('nationaly')->insert(['nombre'=> 'Kazakhstan']);
        DB::table('nationaly')->insert(['nombre'=> 'Kenya']);
        DB::table('nationaly')->insert(['nombre'=> 'Kiribati']);
        DB::table('nationaly')->insert(['nombre'=> 'Kosovo']);
        DB::table('nationaly')->insert(['nombre'=> 'Kuwait']);
        DB::table('nationaly')->insert(['nombre'=> 'Kyrgyzstan']);
        DB::table('nationaly')->insert(['nombre'=> 'Laos']);
        DB::table('nationaly')->insert(['nombre'=> 'Latvia']);
        DB::table('nationaly')->insert(['nombre'=> 'Lebanon']);
        DB::table('nationaly')->insert(['nombre'=> 'Lesotho']);
        DB::table('nationaly')->insert(['nombre'=> 'Liberia']);
        DB::table('nationaly')->insert(['nombre'=> 'Libya']);
        DB::table('nationaly')->insert(['nombre'=> 'Liechtenstein']);
        DB::table('nationaly')->insert(['nombre'=> 'Lithuania']);
        DB::table('nationaly')->insert(['nombre'=> 'Luxembourg']);
        DB::table('nationaly')->insert(['nombre'=> 'Macedonia (FYROM)']);
        DB::table('nationaly')->insert(['nombre'=> 'Madagascar']);
        DB::table('nationaly')->insert(['nombre'=> 'Malawi']);
        DB::table('nationaly')->insert(['nombre'=> 'Malaysia']);
        DB::table('nationaly')->insert(['nombre'=> 'Maldives']);
        DB::table('nationaly')->insert(['nombre'=> 'Mali']);
        DB::table('nationaly')->insert(['nombre'=> 'Malta']);
        DB::table('nationaly')->insert(['nombre'=> 'Marshall Islands']);
        DB::table('nationaly')->insert(['nombre'=> 'Mauritania']);
        DB::table('nationaly')->insert(['nombre'=> 'Mauritius']);
        DB::table('nationaly')->insert(['nombre'=> 'Mexico']);
        DB::table('nationaly')->insert(['nombre'=> 'Micronesia']);
        DB::table('nationaly')->insert(['nombre'=> 'Moldova']);
        DB::table('nationaly')->insert(['nombre'=> 'Monaco']);
        DB::table('nationaly')->insert(['nombre'=> 'Mongolia']);
        DB::table('nationaly')->insert(['nombre'=> 'Montenegro']);
        DB::table('nationaly')->insert(['nombre'=> 'Morocco']);
        DB::table('nationaly')->insert(['nombre'=> 'Mozambique']);
        DB::table('nationaly')->insert(['nombre'=> 'Myanmar']);
        DB::table('nationaly')->insert(['nombre'=> 'Namibia']);
        DB::table('nationaly')->insert(['nombre'=> 'Nauru']);
        DB::table('nationaly')->insert(['nombre'=> 'Nepal']);
        DB::table('nationaly')->insert(['nombre'=> 'Netherlands']);
        DB::table('nationaly')->insert(['nombre'=> 'New Zealand']);
        DB::table('nationaly')->insert(['nombre'=> 'Nicaragua']);
        DB::table('nationaly')->insert(['nombre'=> 'Niger']);
        DB::table('nationaly')->insert(['nombre'=> 'Nigeria']);
        DB::table('nationaly')->insert(['nombre'=> 'North Korea']);
        DB::table('nationaly')->insert(['nombre'=> 'Norway']);
        DB::table('nationaly')->insert(['nombre'=> 'Oman']);
        DB::table('nationaly')->insert(['nombre'=> 'Pakistan']);
        DB::table('nationaly')->insert(['nombre'=> 'Palau']);
        DB::table('nationaly')->insert(['nombre'=> 'Palestine']);
        DB::table('nationaly')->insert(['nombre'=> 'Panama']);
        DB::table('nationaly')->insert(['nombre'=> 'Papua New Guinea']);
        DB::table('nationaly')->insert(['nombre'=> 'Paraguay']);
        DB::table('nationaly')->insert(['nombre'=> 'Peru']);
        DB::table('nationaly')->insert(['nombre'=> 'Philippines']);
        DB::table('nationaly')->insert(['nombre'=> 'Poland']);
        DB::table('nationaly')->insert(['nombre'=> 'Portugal']);
        DB::table('nationaly')->insert(['nombre'=> 'Qatar']);
        DB::table('nationaly')->insert(['nombre'=> 'Romania']);
        DB::table('nationaly')->insert(['nombre'=> 'Russia']);
        DB::table('nationaly')->insert(['nombre'=> 'Rwanda']);
        DB::table('nationaly')->insert(['nombre'=> 'Saint Kitts and Nevis']);
        DB::table('nationaly')->insert(['nombre'=> 'Saint Lucia']);
        DB::table('nationaly')->insert(['nombre'=> 'Saint Vincent and the Grenadines']);
        DB::table('nationaly')->insert(['nombre'=> 'Samoa']);
        DB::table('nationaly')->insert(['nombre'=> 'San Marino']);
        DB::table('nationaly')->insert(['nombre'=> 'Sao Tome and Principe']);
        DB::table('nationaly')->insert(['nombre'=> 'Saudi Arabia']);
        DB::table('nationaly')->insert(['nombre'=> 'Senegal']);
        DB::table('nationaly')->insert(['nombre'=> 'Serbia']);
        DB::table('nationaly')->insert(['nombre'=> 'Seychelles']);
        DB::table('nationaly')->insert(['nombre'=> 'Sierra Leone']);
        DB::table('nationaly')->insert(['nombre'=> 'Singapore']);
        DB::table('nationaly')->insert(['nombre'=> 'Slovakia']);
        DB::table('nationaly')->insert(['nombre'=> 'Slovenia']);
        DB::table('nationaly')->insert(['nombre'=> 'Solomon Islands']);
        DB::table('nationaly')->insert(['nombre'=> 'Somalia']);
        DB::table('nationaly')->insert(['nombre'=> 'South Africa']);
        DB::table('nationaly')->insert(['nombre'=> 'South Korea']);
        DB::table('nationaly')->insert(['nombre'=> 'South Sudan']);
        DB::table('nationaly')->insert(['nombre'=> 'Spain']);
        DB::table('nationaly')->insert(['nombre'=> 'Sri Lanka']);
        DB::table('nationaly')->insert(['nombre'=> 'Sudan']);
        DB::table('nationaly')->insert(['nombre'=> 'Suriname']);
        DB::table('nationaly')->insert(['nombre'=> 'Swaziland']);
        DB::table('nationaly')->insert(['nombre'=> 'Sweden']);
        DB::table('nationaly')->insert(['nombre'=> 'Switzerland']);
        DB::table('nationaly')->insert(['nombre'=> 'Syria']);
        DB::table('nationaly')->insert(['nombre'=> 'Taiwan']);
        DB::table('nationaly')->insert(['nombre'=> 'Tajikistan']);
        DB::table('nationaly')->insert(['nombre'=> 'Tanzania']);
        DB::table('nationaly')->insert(['nombre'=> 'Thailand']);
        DB::table('nationaly')->insert(['nombre'=> 'Timor-Leste']);
        DB::table('nationaly')->insert(['nombre'=> 'Togo']);
        DB::table('nationaly')->insert(['nombre'=> 'Tonga']);
        DB::table('nationaly')->insert(['nombre'=> 'Trinidad and Tobago']);
        DB::table('nationaly')->insert(['nombre'=> 'Tunisia']);
        DB::table('nationaly')->insert(['nombre'=> 'Turkey']);
        DB::table('nationaly')->insert(['nombre'=> 'Turkmenistan']);
        DB::table('nationaly')->insert(['nombre'=> 'Tuvalu']);
        DB::table('nationaly')->insert(['nombre'=> 'Uganda']);
        DB::table('nationaly')->insert(['nombre'=> 'Ukraine']);
        DB::table('nationaly')->insert(['nombre'=> 'United Arab Emirates (UAE)']);
        DB::table('nationaly')->insert(['nombre'=> 'United Kingdom (UK)']);
        DB::table('nationaly')->insert(['nombre'=> 'United States of America (USA)']);
        DB::table('nationaly')->insert(['nombre'=> 'Uruguay']);
        DB::table('nationaly')->insert(['nombre'=> 'Uzbekistan']);
        DB::table('nationaly')->insert(['nombre'=> 'Vanuatu']);
        DB::table('nationaly')->insert(['nombre'=> 'Vatican City']);
        DB::table('nationaly')->insert(['nombre'=> 'Venezuela']);
        DB::table('nationaly')->insert(['nombre'=> 'Vietnam']);
        DB::table('nationaly')->insert(['nombre'=> 'Yemen']);
        DB::table('nationaly')->insert(['nombre'=> 'Zambia']);
        DB::table('nationaly')->insert(['nombre'=> 'Zimbabwe']);
        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nationaly');
    }
}
