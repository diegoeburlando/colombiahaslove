<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
          $table->string('segundo_nombre', 150)->nullable();
          $table->date('fecha_nacimiento');
          $table->string('tipo_documento', 150);
          $table->string('numero_document', 150)->nullable();
          $table->string('direccion', 150)->nullable();
          $table->string('pais', 150)->nullable();
          $table->string('departamento', 150)->nullable();
          $table->string('ciudad', 150)->nullable();
          $table->string('barrio', 150)->nullable();        
          $table->string('telefono', 150)->nullable();
          $table->string('celular', 150)->nullable();
          $table->string('alguna_vez_casado', 150)->nullable();
          $table->string('cuantas_veces', 150)->nullable();
          $table->string('estado_civil', 150);
          $table->string('tiene_hijos', 150)->nullable();
          $table->string('cuantos_hijos', 150)->nullable();
          $table->string('edades_hijos', 150)->nullable();
          $table->string('quiere_hijos', 150);
          $table->string('con_quien_vive', 150)->nullable();
          $table->string('puede_viajar', 150)->nullable();
          $table->string('visa_usa', 150)->nullable();
          $table->string('visa_europa', 150)->nullable();
          $table->string('visa_canada', 150)->nullable();
          $table->string('visa_otro', 150)->nullable();
          $table->string('paises_visitados', 150)->nullable();
          $table->string('actividades_hobbies', 150)->nullable();
          $table->string('profesion', 150)->nullable();
          $table->string('educacion', 150)->nullable();
          $table->string('razon_membresia', 150)->nullable();
          $table->string('como_te_enteraste', 150)->nullable();
          $table->string('ingles', 150)->nullable();
          $table->string('nivel_ingles', 150)->nullable();
          $table->string('espanol', 150)->nullable();
          $table->string('nivel_espanol', 150)->nullable();
          $table->string('frances', 150)->nullable();
          $table->string('nivel_frances', 150)->nullable();
          $table->string('portugues', 150)->nullable();
          $table->string('nivel_portugues', 150)->nullable();
          $table->string('aleman', 150)->nullable();
          $table->string('nivel_aleman', 150)->nullable();
          $table->string('fumas', 150)->nullable();
          $table->string('tomas_licor', 150)->nullable();

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
