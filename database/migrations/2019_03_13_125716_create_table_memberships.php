<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMemberships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_memberships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('meses_3');
            $table->string('meses_6');
            $table->string('meses_12');
            $table->string('tipo_usuario');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_memberships');
    }
}
