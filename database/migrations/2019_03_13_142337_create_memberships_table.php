<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('memberships', function (Blueprint $table) {
               $table->increments('id');
               $table->string('nombre')->nullable();
               $table->string('meses_3')->nullable();
               $table->string('meses_6')->nullable();
               $table->string('meses_12')->nullable();
               $table->string('miembro_a_miembro')->nullable();
               $table->string('acompanamiento')->nullable();
               $table->string('traduccion')->nullable();
               $table->string('acceso_travesia')->nullable();
               $table->string('invitacion_grupo')->nullable();
               $table->string('cantidad_foto')->nullable();
               $table->string('video')->nullable();
               $table->string('filtro_busqueda')->nullable();
               $table->string('esconde_perfil')->nullable();
               $table->string('alertas')->nullable();
               $table->string('tipo_usuario')->nullable();
               $table->string('status')->nullable();
               $table->timestamps();
            });

            DB::table('memberships')->insert([
                'nombre' => 'standar', 
                'meses_3' => 225, 
                'meses_6' => 390, 
                'meses_12' => 660, 
                'miembro_a_miembro' => 0, 
                'acompanamiento' => 1, 
                'traduccion' => 1,
                'acceso_travesia' => 1,
                'invitacion_grupo' => 1,
                'cantidad_foto' => 1,
                'video' => 1,
                'filtro_busqueda' => 0,
                'esconde_perfil' => 0,
                'alertas' => 0,
                'tipo_usuario' => 'male',
                'status' => 1

            ]);

            DB::table('memberships')->insert([
                'nombre' => 'premium', 
                'meses_3' => 285, 
                'meses_6' => 510, 
                'meses_12' => 900, 
                'miembro_a_miembro' => 1, 
                'acompanamiento' => 1, 
                'traduccion' => 1,
                'acceso_travesia' => 1,
                'invitacion_grupo' => 1,
                'cantidad_foto' => 1,
                'video' => 1,
                'filtro_busqueda' => 1,
                'esconde_perfil' => 1,
                'alertas' => 1,
                'tipo_usuario' => 'male',
                'status' => 1

            ]);

            DB::table('memberships')->insert([
                'nombre' => 'annual', 
                'meses_12' => 100, 
                'miembro_a_miembro' => 1, 
                'acompanamiento' => 1, 
                'traduccion' => 1,
                'acceso_travesia' => 1,
                'invitacion_grupo' => 1,
                'cantidad_foto' => 1,
                'video' => 1,
                'filtro_busqueda' => 1,
                'esconde_perfil' => 1,
                'alertas' => 1,
                'tipo_usuario' => 'female',
                'status' => 1

            ]);
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberships');
    }
}
