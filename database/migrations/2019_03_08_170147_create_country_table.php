<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });


        DB::table('country')->insert(['nombre' =>'Afghanistan']);
        DB::table('country')->insert(['nombre' =>'Albania']);
        DB::table('country')->insert(['nombre' =>'Algeria']);
        DB::table('country')->insert(['nombre' =>'Andorra']);
        DB::table('country')->insert(['nombre' =>'Angola']);
        DB::table('country')->insert(['nombre' =>'Antigua and Barbuda']);
        DB::table('country')->insert(['nombre' =>'Argentina']);
        DB::table('country')->insert(['nombre' =>'Armenia']);
        DB::table('country')->insert(['nombre' =>'Australia']);
        DB::table('country')->insert(['nombre' =>'Austria']);
        DB::table('country')->insert(['nombre' =>'Austrian Empire']);
        DB::table('country')->insert(['nombre' =>'Azerbaijan']);
        DB::table('country')->insert(['nombre' =>'Baden']);
        DB::table('country')->insert(['nombre' =>'Bahrain']);
        DB::table('country')->insert(['nombre' =>'Bangladesh']);
        DB::table('country')->insert(['nombre' =>'Barbados']);
        DB::table('country')->insert(['nombre' =>'Bavaria*']);
        DB::table('country')->insert(['nombre' =>'Belarus']);
        DB::table('country')->insert(['nombre' =>'Belgium']);
        DB::table('country')->insert(['nombre' =>'Belize']);
        DB::table('country')->insert(['nombre' =>'Benin (Dahomey)']);
        DB::table('country')->insert(['nombre' =>'Bolivia']);
        DB::table('country')->insert(['nombre' =>'Bosnia and Herzegovina']);
        DB::table('country')->insert(['nombre' =>'Botswana']);
        DB::table('country')->insert(['nombre' =>'Brazil']);
        DB::table('country')->insert(['nombre' =>'Brunei']);
        DB::table('country')->insert(['nombre' =>'Brunswick and Lüneburg']);
        DB::table('country')->insert(['nombre' =>'Bulgaria']);
        DB::table('country')->insert(['nombre' =>'Burkina Faso (Upper Volta)']);
        DB::table('country')->insert(['nombre' =>'Burma']);
        DB::table('country')->insert(['nombre' =>'Burundi']);
        DB::table('country')->insert(['nombre' =>'Cabo Verde']);
        DB::table('country')->insert(['nombre' =>'Cambodia']);
        DB::table('country')->insert(['nombre' =>'Cameroon']);
        DB::table('country')->insert(['nombre' =>'Canada']);
        DB::table('country')->insert(['nombre' =>'Central African Republic']);
        DB::table('country')->insert(['nombre' =>'Central American Federation*']);
        DB::table('country')->insert(['nombre' =>'Chad']);
        DB::table('country')->insert(['nombre' =>'Chile']);
        DB::table('country')->insert(['nombre' =>'China']);
        DB::table('country')->insert(['nombre' =>'Colombia']);
        DB::table('country')->insert(['nombre' =>'Comoros']);
        DB::table('country')->insert(['nombre' =>'Costa Rica']);
        DB::table('country')->insert(['nombre' =>'Cote d’Ivoire (Ivory Coast)']);
        DB::table('country')->insert(['nombre' =>'Croatia']);
        DB::table('country')->insert(['nombre' =>'Cuba']);
        DB::table('country')->insert(['nombre' =>'Cyprus']);
        DB::table('country')->insert(['nombre' =>'Czechia']);
        DB::table('country')->insert(['nombre' =>'Czechoslovakia']);
        DB::table('country')->insert(['nombre' =>'Democratic Republic of the Congo']);
        DB::table('country')->insert(['nombre' =>'Denmark']);
        DB::table('country')->insert(['nombre' =>'Djibouti']);
        DB::table('country')->insert(['nombre' =>'Dominica']);
        DB::table('country')->insert(['nombre' =>'Dominican Republic']);
        DB::table('country')->insert(['nombre' =>'East Germany (German Democratic Republic)']);
        DB::table('country')->insert(['nombre' =>'Ecuador']);
        DB::table('country')->insert(['nombre' =>'Egypt']);
        DB::table('country')->insert(['nombre' =>'El Salvador']);
        DB::table('country')->insert(['nombre' =>'Equatorial Guinea']);
        DB::table('country')->insert(['nombre' =>'Eritrea']);
        DB::table('country')->insert(['nombre' =>'Estonia']);
        DB::table('country')->insert(['nombre' =>'Eswatini']);
        DB::table('country')->insert(['nombre' =>'Ethiopia']);
        DB::table('country')->insert(['nombre' =>'Finland']);
        DB::table('country')->insert(['nombre' =>'France']);
        DB::table('country')->insert(['nombre' =>'Gabon']);
        DB::table('country')->insert(['nombre' =>'Georgia']);
        DB::table('country')->insert(['nombre' =>'Germany']);
        DB::table('country')->insert(['nombre' =>'Ghana']);
        DB::table('country')->insert(['nombre' =>'Greece']);
        DB::table('country')->insert(['nombre' =>'Grenada']);
        DB::table('country')->insert(['nombre' =>'Guatemala']);
        DB::table('country')->insert(['nombre' =>'Guinea']);
        DB::table('country')->insert(['nombre' =>'Guinea-Bissau']);
        DB::table('country')->insert(['nombre' =>'Guyana']);
        DB::table('country')->insert(['nombre' =>'Haiti']);
        DB::table('country')->insert(['nombre' =>'Hanover*']);
        DB::table('country')->insert(['nombre' =>'Hanseatic Republics*']);
        DB::table('country')->insert(['nombre' =>'Hawaii*']);
        DB::table('country')->insert(['nombre' =>'Hesse*']);
        DB::table('country')->insert(['nombre' =>'Holy See']);
        DB::table('country')->insert(['nombre' =>'Honduras']);
        DB::table('country')->insert(['nombre' =>'Hungary']);
        DB::table('country')->insert(['nombre' =>'Iceland']);
        DB::table('country')->insert(['nombre' =>'India']);
        DB::table('country')->insert(['nombre' =>'Indonesia']);
        DB::table('country')->insert(['nombre' =>'Iran']);
        DB::table('country')->insert(['nombre' =>'Iraq']);
        DB::table('country')->insert(['nombre' =>'Ireland']);
        DB::table('country')->insert(['nombre' =>'Israel']);
        DB::table('country')->insert(['nombre' =>'Italy']);
        DB::table('country')->insert(['nombre' =>'Jamaica']);
        DB::table('country')->insert(['nombre' =>'Japan']);
        DB::table('country')->insert(['nombre' =>'Jordan']);
        DB::table('country')->insert(['nombre' =>'Kazakhstan']);
        DB::table('country')->insert(['nombre' =>'Kenya']);
        DB::table('country')->insert(['nombre' =>'Kiribati']);
        DB::table('country')->insert(['nombre' =>'Korea']);
        DB::table('country')->insert(['nombre' =>'Kosovo']);
        DB::table('country')->insert(['nombre' =>'Kuwait']);
        DB::table('country')->insert(['nombre' =>'Kyrgyzstan']);
        DB::table('country')->insert(['nombre' =>'Laos']);
        DB::table('country')->insert(['nombre' =>'Latvia']);
        DB::table('country')->insert(['nombre' =>'Lebanon']);
        DB::table('country')->insert(['nombre' =>'Lesotho']);
        DB::table('country')->insert(['nombre' =>'Lew Chew (Loochoo)*']);
        DB::table('country')->insert(['nombre' =>'Liberia']);
        DB::table('country')->insert(['nombre' =>'Libya']);
        DB::table('country')->insert(['nombre' =>'Liechtenstein']);
        DB::table('country')->insert(['nombre' =>'Lithuania']);
        DB::table('country')->insert(['nombre' =>'Luxembourg']);
        DB::table('country')->insert(['nombre' =>'Macedonia']);
        DB::table('country')->insert(['nombre' =>'Madagascar']);
        DB::table('country')->insert(['nombre' =>'Malawi']);
        DB::table('country')->insert(['nombre' =>'Malaysia']);
        DB::table('country')->insert(['nombre' =>'Maldives']);
        DB::table('country')->insert(['nombre' =>'Mali']);
        DB::table('country')->insert(['nombre' =>'Malta']);
        DB::table('country')->insert(['nombre' =>'Marshall Islands']);
        DB::table('country')->insert(['nombre' =>'Mauritania']);
        DB::table('country')->insert(['nombre' =>'Mauritius']);
        DB::table('country')->insert(['nombre' =>'Mecklenburg-Schwerin*']);
        DB::table('country')->insert(['nombre' =>'Mecklenburg-Strelitz*']);
        DB::table('country')->insert(['nombre' =>'Mexico']);
        DB::table('country')->insert(['nombre' =>'Micronesia']);
        DB::table('country')->insert(['nombre' =>'Moldova']);
        DB::table('country')->insert(['nombre' =>'Monaco']);
        DB::table('country')->insert(['nombre' =>'Mongolia']);
        DB::table('country')->insert(['nombre' =>'Montenegro']);
        DB::table('country')->insert(['nombre' =>'Morocco']);
        DB::table('country')->insert(['nombre' =>'Mozambique']);
        DB::table('country')->insert(['nombre' =>'Namibia']);
        DB::table('country')->insert(['nombre' =>'Nassau*']);
        DB::table('country')->insert(['nombre' =>'Nauru']);
        DB::table('country')->insert(['nombre' =>'Nepal']);
        DB::table('country')->insert(['nombre' =>'New Zealand']);
        DB::table('country')->insert(['nombre' =>'Nicaragua']);
        DB::table('country')->insert(['nombre' =>'Niger']);
        DB::table('country')->insert(['nombre' =>'Nigeria']);
        DB::table('country')->insert(['nombre' =>'North German Confederation*']);
        DB::table('country')->insert(['nombre' =>'North German Union*']);
        DB::table('country')->insert(['nombre' =>'Norway']);
        DB::table('country')->insert(['nombre' =>'Oldenburg*']);
        DB::table('country')->insert(['nombre' =>'Oman']);
        DB::table('country')->insert(['nombre' =>'Orange Free State*']);
        DB::table('country')->insert(['nombre' =>'Pakistan']);
        DB::table('country')->insert(['nombre' =>'Palau']);
        DB::table('country')->insert(['nombre' =>'Panama']);
        DB::table('country')->insert(['nombre' =>'Papal States*']);
        DB::table('country')->insert(['nombre' =>'Papua New Guinea']);
        DB::table('country')->insert(['nombre' =>'Paraguay']);
        DB::table('country')->insert(['nombre' =>'Peru']);
        DB::table('country')->insert(['nombre' =>'Philippines']);
        DB::table('country')->insert(['nombre' =>'Piedmont-Sardinia*']);
        DB::table('country')->insert(['nombre' =>'Poland']);
        DB::table('country')->insert(['nombre' =>'Portugal']);
        DB::table('country')->insert(['nombre' =>'Qatar']);
        DB::table('country')->insert(['nombre' =>'Republic of Korea (South Korea)']);
        DB::table('country')->insert(['nombre' =>'Republic of the Congo']);
        DB::table('country')->insert(['nombre' =>'Romania']);
        DB::table('country')->insert(['nombre' =>'Russia']);
        DB::table('country')->insert(['nombre' =>'Rwanda']);
        DB::table('country')->insert(['nombre' =>'Saint Kitts and Nevis']);
        DB::table('country')->insert(['nombre' =>'Saint Lucia']);
        DB::table('country')->insert(['nombre' =>'Saint Vincent and the Grenadines']);
        DB::table('country')->insert(['nombre' =>'Samoa']);
        DB::table('country')->insert(['nombre' =>'San Marino']);
        DB::table('country')->insert(['nombre' =>'Sao Tome and Principe']);
        DB::table('country')->insert(['nombre' =>'Saudi Arabia']);
        DB::table('country')->insert(['nombre' =>'Schaumburg-Lippe*']);
        DB::table('country')->insert(['nombre' =>'Senegal']);
        DB::table('country')->insert(['nombre' =>'Serbia']);
        DB::table('country')->insert(['nombre' =>'Seychelles']);
        DB::table('country')->insert(['nombre' =>'Sierra Leone']);
        DB::table('country')->insert(['nombre' =>'Singapore']);
        DB::table('country')->insert(['nombre' =>'Slovakia']);
        DB::table('country')->insert(['nombre' =>'Slovenia']);
        DB::table('country')->insert(['nombre' =>'Somalia']);
        DB::table('country')->insert(['nombre' =>'South Africa']);
        DB::table('country')->insert(['nombre' =>'South Sudan']);
        DB::table('country')->insert(['nombre' =>'Spain']);
        DB::table('country')->insert(['nombre' =>'Sri Lanka']);
        DB::table('country')->insert(['nombre' =>'Sudan']);
        DB::table('country')->insert(['nombre' =>'Suriname']);
        DB::table('country')->insert(['nombre' =>'Sweden']);
        DB::table('country')->insert(['nombre' =>'Switzerland']);
        DB::table('country')->insert(['nombre' =>'Syria']);
        DB::table('country')->insert(['nombre' =>'Tajikistan']);
        DB::table('country')->insert(['nombre' =>'Tanzania']);
        DB::table('country')->insert(['nombre' =>'Thailand']);
        DB::table('country')->insert(['nombre' =>'The Bahamas']);
        DB::table('country')->insert(['nombre' =>'The Cayman Islands']);
        DB::table('country')->insert(['nombre' =>'The Congo Free State']);
        DB::table('country')->insert(['nombre' =>'The Duchy of Parma*']);
        DB::table('country')->insert(['nombre' =>'The Gambia']);
        DB::table('country')->insert(['nombre' =>'The Grand Duchy of Tuscany*']);
        DB::table('country')->insert(['nombre' =>'The Netherlands']);
        DB::table('country')->insert(['nombre' =>'The Solomon Islands']);
        DB::table('country')->insert(['nombre' =>'The United Arab Emirates']);
        DB::table('country')->insert(['nombre' =>'The United Kingdom']);
        DB::table('country')->insert(['nombre' =>'Timor-Leste']);
        DB::table('country')->insert(['nombre' =>'Togo']);
        DB::table('country')->insert(['nombre' =>'Tonga']);
        DB::table('country')->insert(['nombre' =>'Trinidad and Tobago']);
        DB::table('country')->insert(['nombre' =>'Tunisia']);
        DB::table('country')->insert(['nombre' =>'Turkey']);
        DB::table('country')->insert(['nombre' =>'Turkmenistan']);
        DB::table('country')->insert(['nombre' =>'Tuvalu']);
        DB::table('country')->insert(['nombre' =>'Two Sicilies*']);
        DB::table('country')->insert(['nombre' =>'Uganda']);
        DB::table('country')->insert(['nombre' =>'Ukraine']);
        DB::table('country')->insert(['nombre' =>'Union of Soviet Socialist Republics*']);
        DB::table('country')->insert(['nombre' =>'Uruguay']);
        DB::table('country')->insert(['nombre' =>'Uzbekistan']);
        DB::table('country')->insert(['nombre' =>'Vanuatu']);
        DB::table('country')->insert(['nombre' =>'Venezuela']);
        DB::table('country')->insert(['nombre' =>'Vietnam']);
        DB::table('country')->insert(['nombre' =>'Yemen']);
        DB::table('country')->insert(['nombre' =>'Zambia']);
        DB::table('country')->insert(['nombre' =>'Zimbabwe']);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country');
    }
}
