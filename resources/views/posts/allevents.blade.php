@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="img-box">
			<img src="http://colombiahaslove.com/wp-content/uploads/2019/01/banner1.jpg" alt="">		
		</div>
	</div>
</div>
<section class="all-testimonies">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div class="title-shadow-content">
					<div class="title-shadow">
						EVENTS
					</div>
					<div class="title-shadow-text">
						<h3>EVENTS</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				@for($i = 0; count($events) > $i; $i++)
				<div class="col-md-3">
					<div class="card">
						<img class="card-img-top" src="{{images($events[$i]->img)}}" alt="">
						<div class="card-body">
							<h6 class="card-title">
								{{cortar($events[$i]->titulo, 20)}}
							</h6>
							<a href="{{ route('single-event', ['id' => $events[$i]->id])}}" class="btn btn-primary btn-sm">SEE MORE</a>
						</div>
					</div>
				</div>
				@endfor
			</div>
		</div>
		
	</div>
</section>

@endsection