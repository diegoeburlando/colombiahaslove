@extends('layouts/admin')

@section('content')
  <div class="row">
      <div class="col-lg-12 margin-tb">
          <div class="pull-left">
              <h2> Show Post</h2>
          </div>
          <div class="pull-right">
              <br/>
              <a class="btn btn-primary" href="{{ route('posts.index') }}"><i class="fas fa-arrow-left"></i></a>
          </div>
      </div>
  </div>
  <div class="container">
    <div class="row">
      <form  action="{{ route('posts.update', $post->id)}}" method="POST" role="form">
         @csrf
          @method('PUT')
        <div class="col-xs-12 col-md-12">
            <div class="form-group">
                <input class="form-control" type="text" name="titulo" id="titulo" value="{{ $post->titulo}}">
            </div>
        </div>
        <div class="col-xs-12 col-md-12">
            <div class="form-group">
                <textarea class="form-control" name="descripcion" id="descripcion" cols="30" rows="10">{{ $post->descripcion}}</textarea>
            </div>
        </div>
        <script>CKEDITOR.replace( 'descripcion' );</script>
        <div class="col-md-6 col-xs-12">
          <select class="form-control" name="categoria" id="categoria">
            @foreach($categorias as $categoria)
              @if($categoria->nombre == $post->categoria)
                <option value="{{$categoria->nombre}}" selected>{{$categoria->nombre}}</option>
              @else
                <option value="{{$categoria->nombre}}">{{$categoria->nombre}}</option>
              @endif
            @endforeach
          </select>
        </div>
        <div class="col-md-6 col-xs-12">
          <button type="submit" class="btn btn-success">EDIT</button>
        </div>
        </form>
    </div>
  </div>
@endsection