@extends('layouts/admin')

@section('content')

  <div class="row justify-content-center">
    <div class="col-md-8">
      <form enctype="multipart/form-data" action="{{route('posts.store')}}" method="post">
        @csrf
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="">Title</label>
              <input name="titulo" type="text" class="form-control" placeholder="title" maxlength="40" required>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label for="">Description</label>
              <textarea name="descripcion" class="form-control" id="editor1" rows="6" placeholder="content" required></textarea>
            </div>
          </div>
          <script>CKEDITOR.replace( 'editor1' );</script>
          <div class="col-md-6">
            <div class="form-group">
              <select class="form-control" name="categoria" id="categoria" required>
                  <option disabled selected>category</option>
                @foreach($categoria as $cat)
                  <option value="{{$cat->nombre}}">{{$cat->nombre}}</option>
                @endforeach
              </select>
            </div>
          </div>
           <div class="col-md-12">
            <div class="form-group">
              <label for="">Image</label>
              <input name="file" type="file" class="form-control" placeholder="file" required>
            </div>
          </div>
          <div class="col-md-6">
              <button type="submit" class="btn btn-success btn-block">save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection