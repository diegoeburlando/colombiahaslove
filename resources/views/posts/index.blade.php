@extends('layouts/admin')

@section('content')
  <div class="row">
  <div class="col-sm-12">
    <div class="full-right">
      <h2>All Posts</h2>
    </div>
  </div>
  </div>

  @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
  @endif

  <table class="table table-bordered">
    <tr>
      <th with="80px">id</th>
      <th>Title</th>
      <th>Description</th>
      <th>Category</th>
      <th>Image</th>
      <th with="140px" class="text-center">
        <a href="{{route('posts.create')}}" class="btn btn-success btn-sm">
          <i class="fas fa-plus"></i>
        </a>
      </th>
    </tr>
    <?php $no=1; ?>
    @foreach ($posts as $key => $value)
      <tr>
        <td>{{$no++}}</td>
        <td>{{ $value->titulo }}</td>
        <td>{{ cortar($value->descripcion, 50) }}</td>
        <td>{{ $value->categoria }}</td>
        <td><div class="preview-table"><img class="img-fluid" src="{{ $value->img }}" alt=""></div></td>
        <td>
          <a class="btn btn-primary btn-sm" href="{{route('posts.edit',$value->id)}}">
             <i class="fas fa-pencil-alt"></i>
         </a>
        <form action="{{route('posts.destroy', ['id' => $value->id])}}" method="POST">
        	@csrf

        @method('DELETE')
          <button type="submit" style="display: inline;" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></button>
        </form>
        </td>
      </tr>
    @endforeach
  </table>
  {{ $posts->links() }}
@endsection