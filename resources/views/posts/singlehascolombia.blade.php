@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="img-box">
			<img src="http://colombiahaslove.com/wp-content/uploads/2019/01/banner1.jpg" alt="">		
		</div>
	</div>
</div>
<section class="single-testimonies">
	<div class="container">
		<div class="row">
			<div class="col-md-8 mx-auto col-xs-12">
				<div class="card shadow">
					<img src="{{images($hascolombia->img)}}" alt="" class="card-img-top">
					<div class="card-body">
						<h4 class="card-title">
							{{$hascolombia->titulo}}
						</h4>
						<p>
							{{$hascolombia->descripcion}}
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection