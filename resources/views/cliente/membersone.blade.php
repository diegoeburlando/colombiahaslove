@extends('layouts/app')

@section('content')
<section class="carousel-members">
			<div class="img-box">
				<img src="{{asset('img/image-banner.jpg')}}" alt="">		
			</div>
	<div class="container MAIN">
			<div class="row">
				<div class="col-md-12 title-ours-members">
					<h3>OURS</h3> <h3>MEMBERS</h3>
		 		</div>
		 		<div class="col-md-12">
		 			<p class="p-ours-members">
		 					Registered members in Colombia have love are kind, reliable and serious people. We are interested in crossing borders and giving the possibility to people from different parts of the world to meet
		 			</p>
		 		</div>
			</div>
			<div class="row box-carousel-members justify-content-center">
				<div class="col-md-1 d-flex justify-content-end flechas align-items-center">
					<img class="img-prev" src="{{asset('img/arrow-left.png')}}" alt="">
				</div>
					<div class="col-md-7">
						<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
						  <div class="carousel-inner">
						  	@foreach($person as $key => $p)
						  	@php
						  		$active = "";
						  		if($key==0){$active = "active";}else{$active = "";} 
						  	@endphp
						    <div class="carousel-item {{$active}}">
						      <img class="d-block w-100 shadow" src="{{asset('img')}}/{{$p->profile_img}}" alt="First slide">
						      <div class="content-carousel-members">
						          <h5>{{ucwords($p->name)}} {{ucwords($p->lastname)}}</h5>
						          <p>{{ucwords($p->profesion)}}</p>
						          <small>{{ucwords($p->ciudad)}} - {{ucwords($p->pais)}}</small>
						        </div>
						    </div>
						    @endforeach
						  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>		
					</div>
				</div>
				<div class="col-md-1 d-flex justify-content-start flechas align-items-center">
					<img class="img-next" src="{{asset('img/arrow-right.png')}}" alt="">
				</div>
				<div class="col-md-7">
					<div class="row justify-content-end panel-sign-up">
						<div class="col-md-7">
							<h6><strong>Would you like to know more about this person?</strong></h6>
							<p>Subscribe to the premium membership</p>
							<a href="#" class="btn btn-primary">Sign Up</a>
						</div>
					</div>
				</div>
			</div>

	</div>
</section>



@endsection











































