@extends('layouts.app')

@section('content')
<div class="img-box">
	<img src="{{asset('img/image-banner.jpg')}}" alt="">		
</div>
<section class="profile">
	<div class="container MAIN">
		<div class="row">
			<div class="col-md-12">
				<div class="title-shadow-content">
					<div class="title-shadow">
						PROFILE
					</div>
					<div class="title-shadow-text">
						<h3>MY</h3><h3>PROFILE</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body total">
						<div class="index-1 container">
							<div class="row">
								<div class="col-md-4">
									<div class="card">
										<div class="card-header bg-primary text-white">
											<i class="fas fa-users"></i> Menu 
										</div>
										<ul class="menu-profile">
											<li class="item-menu  active" id="profile">PROFILE</li>
											<li class="item-menu" id="photos">MY PHOTOS</li>
											<li class="item-menu" id="information">MY INFORMATION</li>
											<li class="item-menu" id="friends">MY FRIENDS</li>
										</ul>
									</div>
								</div>
								<div class="col-md-8 profile-element">
									<div class="card icard active profile">
										<div class="row">
											<div class="col-md-3 content-profile-image">
												@if(!empty($data->profile_img))
													<img  src="{{$data->profile_img}}" alt="" index="foto0" class="img-fluid m-3 rounded-circle">
												@else
													<img  src="{{asset('/img/no-image-profile.jpg')}}" index="foto0" alt="" class="card-img-top">
												@endif
											</div>
											<div class="col-md-5">
												<div class=" p-3">
													<h4>{{ucwords(Auth::user()->name)}}</h4>
													@if($data->profesion)
														<p>{{ucwords($data->profesion)}}</p>
													@endif
													<p>Birthdate: 13 de marzo</p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="card">
													<div class="card-header bg-primary text-white">
														<i class="fas fa-users"></i> FRIENDS REQUEST
													</div>
													<div class="card-body">
														<small>no request</small>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="card icard photos">
										<div class="row">
											<div class="col-md-10 mx-auto">
												<div class="row p-3">
													<div class="col-md-3">
														<div class="card">
															<div class="card-header bg-primary text-white">
																MAIN
															</div>
															<div class="card-body">
																<div class="card">
																	@if(!empty($data->profile_img))
																		<img  src="{{$data->profile_img}}" alt="" index="foto0" class="card-img-top">
																	@else
																		<img  src="{{asset('/img/no-image-profile.jpg')}}" index="foto0" alt="" class="card-img-top">
																	@endif
																</div>
															</div>
														</div>
													</div>
													@php
														$photos = explode(',', $data->galeria);
														
													@endphp
													<div class="col-md-9">
														<div class="card">
															<div class="card-header bg-primary text-white">
																GALLERY IMAGES
															</div>
															<div class="card-body photo-galeria">
																<div class="row">
																	<div class="col-md-3">
																		<div class="card">
																			@if(!empty($data->foto1))
																				<img  src="{{$data->foto1}}" alt="" index="foto1" class="card-img-top ">
																			@else
																				<img  src="{{asset('/img/no-image-profile.jpg')}}" index="foto1" alt="" class="card-img-top">
																			@endif
																		</div>
																	</div>
																	<div class="col-md-3">
																		<div class="card">
																			@if(!empty($data->foto2))
																				<img  src="{{$data->foto2}}" alt="" index="foto2" class="card-img-top">
																			@else
																				<img  src="{{asset('/img/no-image-profile.jpg')}}" index="foto2" alt="" class="card-img-top">
																			@endif
																		</div>
																	</div>
																	<div class="col-md-3">
																		<div class="card">
																			@if(!empty($data->foto3))
																				<img  src="{{$data->foto3}}" alt="" index="foto3" class="card-img-top ">
																			@else
																				<img  src="{{asset('/img/no-image-profile.jpg')}}" index="foto3"  alt="" class="card-img-top">
																			@endif
																		</div>
																	</div>
																	<div class="col-md-3">
																		<div class="card">
																			@if(!empty($data->foto4))
																				<img src="{{$data->foto4}}" alt="" index="foto4" class="card-img-top ">
																			@else
																				<img src="{{asset('/img/no-image-profile.jpg')}}" index="foto4" alt="" class="card-img-top">
																			@endif
																		</div>
																	</div>
																</div>
															</div>
															<form enctype="multipart/form-data" action="{{route('savephoto', ['id' => $data->id])}}" method="POST">
																@method('POST')
																@csrf
																<input name="foto0" id="foto0" type="file">
																<input name="foto1" id="foto1" type="file">
																<input name="foto2" id="foto2" type="file">
																<input name="foto3" id="foto3" type="file">
																<input name="foto4" id="foto4" type="file">
																<input name="user_id" value="{{$data->user_id}}" type="hidden">
																<button type="submit" class="btn btn-primary">save</button>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="card icard information">
										<div class="card-header bg-primary text-white">
											MY INFORMATION
										</div>
										<div class="card-body">
										 	<form enctype="multipart/form-data" action="{{route('adminstore')}}" method="post" class="add-user" autocomplete="off">
										 	    @csrf
										 	  	<div class="row">
										 	  		<div class="col-md-6 col-xs-12">
										 	  			<div class="form-group">
										 	  				<label for="">Names<span class="text-danger">*</span></label>
										 	  					<input type="text" class="form-control" id="name" name="name" value="{{$data->name}}" aria-describedby="" placeholder="name" autofocus="" autocomplete="off" disabled required>
										 	  			</div>
										 	  		</div>
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	  						   <label for="">Lastname<span class="text-danger">*</span></label>
							 	  						   <input type="text" class="form-control" id="lastname" name="lastname"  value="{{$data->lastname}}" aria-describedby="" placeholder="lastname" autofocus="" required>
							 	  						 </div>
							 	  					</div>
							 	  					{{-- <div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	  						   <label for="">Email<span class="text-danger">*</span></label>
							 	  						   <input type="email" class="form-control" id="email" name="email"  value="{{$data->email}}" aria-describedby="" placeholder="email" autofocus="" required>
							 	  						 </div>
							 	  					</div> --}}
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	  						   <label for="">Birthday<span class="text-danger">*</span></label>
							 	  						   <input type="text" class="form-control" name="fecha_nacimiento"  id="fecha_nacimiento" value="{{$data->fecha_nacimiento}}" aria-describedby="" placeholder="birthday" autofocus="" required>
							 	  						 </div>
							 	  					</div>
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	        					<label for="">Document<span class="text-danger">*</span></label>
							 	        					<select class="form-control" name="tipo_documento" id="tipo_documento" required>
								 	                          <option value="" disabled="" selected="">choose</option>
								 	                          <option value="id">Id card</option>
								 	                          <option value="passport">Passport</option>
							 	                   		 	</select>
							 	  						 </div>
							 	  					</div>
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	  						   <label for="">Document Number<span class="text-danger">*</span></label>
							 	  						   <input type="text" class="form-control" id="numero_document" name="numero_document" aria-describedby="" placeholder="number" autofocus="" maxlength="15" required>
							 	  						 </div>
							 	  					</div>
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	  						   <label for="">Nationality<span class="text-danger">*</span></label>
							 	  						   <input type="text" name="nationality" id="nationality" disabled="" class="form-control" value="{{$data->nationality}}">
							 	  						   {{-- <select name="nationality" id="nationality" class="form-control" required>
										 	                    <option value="" disabled selected>choose</option>
										 	                    @foreach($nacionalidad as $pais)
										 	                    <option value="{{$pais->nombre}}">{{$pais->nombre}}</option>
							 	                    			@endforeach    
							 	                			</select> --}}
							 	  						 </div>
							 	  					</div>
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	  						   <label for="">Gender<span class="text-danger">*</span></label>
							 	  						   <select class="form-control" name="gender" name="gender" id="gender" required>
							 	                       			<option value="" disabled selected>choose</option>
									 	                       @foreach($gender as $key => $g)
									 	                        @if($key == 2) @break @endif
									 	                        <option value="{{$g->nombre}}">{{$g->nombre}}</option>
									 	                       @endforeach
							 	                		 	</select>
							 	  						 </div>
							 	  					</div>
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	                 			<label for="">Interesed<span class="text-danger">*</span></label>
								 	                 		<select class="form-control" name="interesed" id="interesed" required>
								 	                       		<option value="" disabled selected>choose</option>
									 	                       @foreach($gender as $g)
									 	                          <option value="{{$g->nombre}}">{{$g->nombre}}</option>
									 	                       @endforeach
								 	                 		</select>
							 	               			</div>
							 	  					</div>
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	  						   <label for="">Address<span class="text-danger">*</span></label>
							 	  						   <input type="text" class="form-control" name="direccion" id="direccion"  aria-describedby="" placeholder="" autofocus="" required>
							 	  						 </div>
							 	  					</div>
										 	  		<div class="col-md-6 col-xs-12">
										 	  			<div class="form-group">
										 	  				<label for="">Country<span class="text-danger">*</span></label>
										 	  				<select class="form-control" name="pais" id="pais" required>
										 	                    <option value="" disabled selected>choose</option>
										 	                      @foreach($p as $value)
										 	                          <option value="{{$value->id}}">{{$value->nombre}}</option>
										 	                       @endforeach  
										 	               	</select>
										 	  			</div>
										 	  		</div>
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	  						   <label for="">City<span class="text-danger">*</span></label>
							 	  						   <input type="text" class="form-control" name="ciudad" id="ciudad" aria-describedby="" placeholder="name city" autofocus="" required>
							 	  						 </div>
							 	  					</div>
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	  						   <label for="">Town</label>
							 	  						   <input type="text" class="form-control" name="barrio" id="barrio" aria-describedby="" placeholder="name town" autofocus="">
							 	  						 </div>
							 	  					</div>
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	  						   <label for="">Phone</label>
							 	  						   <input type="text" class="form-control"  name="telefono" id="telefono aria-describedby="" placeholder="number phone" autofocus="">
							 	  						 </div>
							 	  					</div>
							 	  					<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	  						   <label for="">Cellphone<span class="text-danger">*</span></label>
							 	  						   <input type="text" class="form-control" name="cellphone" id="cellphone" aria-describedby="" placeholder="number cellphone" autofocus="" required>
							 	  						 </div>
							 	  					</div>
										 	  		<div class="col-md-6 col-xs-12">
										 	  			<div class="form-group">
										 	                <div class="row">
										 	                    <div class="col-md-6 col-xs-12">
										 	  						<label for="">Ever Married?</label>
											 	                      <select name="alguna_vez_casado" id="alguna_vez_casado" class="form-control">
											 	                        <option value="no" selected="">No</option>
											 	                        <option value="yes">Yes</option>
											 	                      </select>
										 	                    </div>
										 	                    <div class="col-md-6 col-xs-12">
										 	                      <label for="">Who many married?</label>
										 	                      <select name="cuantos_hijos" id="cuantos_hijos" class="form-control" disabled>
										 	                        <option disabled selected>0</option>
										 	                        <option value="1">1</option> 
										 	                        <option value="2">2</option>
										 	                        <option value="3">3</option>
										 	                        <option value="4">4</option> 
										 	                        <option value="5">5</option>                                
										 	                      </select>
										 	                    </div>
										 	                  </div>
										 	              </div>                          
										 	  		</div>
									 	            <div class="col-md-6 col-xs-12">
									 	              <div class="form-group">
									 	                  <div class="row">
									 	                    <div class="col-md-6 col-xs-12">
									 	                      <label for="">You have children?</label>
									 	                      <select name="tiene_hijos" id="tiene_hijos" class="form-control">
									 	                        <option value="no" selected="">No</option>
									 	                        <option value="yes">Yes</option>
									 	                      </select>
									 	                    </div>
									 	                    <div class="col-md-6 col-xs-12">
									 	                      <label for="">How many children?</label>
									 	                      <select name="cuantas_veces" id="cuantas_veces" class="form-control" disabled>
									 	                        <option disabled selected>0</option>
									 	                        <option value="1">1</option> 
									 	                        <option value="2">2</option>
									 	                        <option value="3">3</option>
									 	                        <option value="4">4</option> 
									 	                        <option value="5">5</option>                                
									 	                      </select>
									 	                    </div>
									 	                  </div>
									 	              </div>                          
									 	            </div>
								 	  				<div class="col-md-6 col-xs-12">
							 	  						<div class="form-group">
							 	      						<label for="">Civil Status</label>
							 							      <select class="form-control"  id="estado_civil" name="estado_civil" disabled>
							 	                     			 <option value="" disabled selected>choose</option>
							 	                 			 </select>
							 	  						 </div>
								 	  				</div>
													<div class="col-md-6 col-xs-12">
						 	  						   <label for="">Who do you live?</label>
						 	  						   <input type="text" class="form-control" name="con_quien_vive" id="con_quien_vive" aria-describedby="" placeholder="" autofocus="">
								 	  				</div>
								 	  				
										 	  		<div class="col-md-6 col-xs-12">
										 	  			<div class="form-group">
										 	  				<label for="">Do you want childrens?</label>
										 	      			<select class="form-control" name="quiere_hijos" id="quiere_hijos">
										 	                    <option value="" disabled selected>choose</option>
										 	                    <option value="yes">yes</option>
										 	                    <option value="no">no</option>                              
										 	                 </select>
										 	  			</div>
										 	  		</div>
										 	  		<div class="col-md-6 col-xs-12">
										 	            <label for="">Flexibility to travel?</label>
										 	             <select class="form-control" name="puede_viajar" id="puede_viajar">
										 	                <option value="" disabled selected>choose</option>
										 	                <option value="yes">yes</option>
										 	                 <option value="no">no</option>                              
										 	             </select>
										 	  		</div>
										 	  		<div class="col-md-6 col-xs-12">
										 	  			<div class="form-group">
										 	  				<label for="">Count country visited</label>
										 	  				<select class="form-control" name="paises_visitados" id="paises_visitados">
										 	                    <option value="0" select>0</option> 
										 	                    <option value="1">1</option> 
										 	                    <option value="2">2</option>
										 	                    <option value="3">3</option>
										 	                    <option value="4">4</option> 
										 	                    <option value="5">5</option>                                
										 	                    <option value="6">6</option> 
										 	                    <option value="7">7</option> 
										 	                    <option value="8">8</option> 
										 	                    <option value="9">9</option> 
										 	                    <option value="10">10</option> 
										 	                 </select>
										 	  			</div>
										 	  		</div>
										 	  		<div class="col-md-6 col-xs-12">
										 	  			<div class="form-group">
										 	  				<label for="">Hobbies</label>
										 	  				<select name="actividades_hobbies" id="name="actividades_hobbies" class="form-control">
										 	                	<option disabled selected>choose</option>
										 	                    <option value="sport">Sport</option> 
										 	                    <option value="write">Writen</option>      
										 	                 </select>
										 	  			</div>
										 	  		</div>
										 	  		<div class="col-md-6 col-xs-12">
										 	  			<div class="form-group">
										 	    			<label for="">Religion</label>
										 	    				<select class="form-control" name="religion" id="religion">
										 	                      <option value="" selected disabled="">choose</option> 
										 	                      <option value="catholic">catholic</option> 
										 	                      <option value="muslim">muslim</option>
										 	                      <option value="atheist">atheist</option>
										 	                      <option value="jew">jew</option> 
										 	                      <option value="hindu">hindu</option>                                
										 	                      <option value="buddha">buddha</option>
										 	                      <option value="other">other</option> 
										 	               		</select>
										 	  			</div>
										 	  		</div>
										 	        <div class="col-md-6 col-xs-12">
										 	            <div class="form-group">
										 	                <label for="">Profession</label>
										 	                <select class="form-control" name="profesion" id="profesion">
										 	                    <option selected disabled>choose</option> 
										 	                    <option value="engineer">Engineer</option> 
									 	                        <option value="manager">Manager</option> 
									 	                        <option value="teacher">Teacher</option>
										 	                </select>
										 	            </div>
										 	        </div>
										 	        <div class="col-md-6 col-xs-12">
									 	              <div class="form-group">
									 	                <label for="">Height</label>
									 	                <select class="form-control" name="estatura" id="estatura">
									 	                     <option selected disabled>choosee</option> 
									 	                     <option value="100">100cm</option> 
									 	                     <option value="110">110cm</option> 
									 	                     <option value="120">120cm</option> 
									 	                     <option value="130">130cm</option> 
									 	                     <option value="140">140cm</option> 
									 	                     <option value="150">150cm</option>
									 	                     <option value="160">160cm</option> 
									 	                     <option value="170">170cm</option> 
									 	                     <option value="180">180cm</option> 
									 	                     <option value="190">190cm</option> 
									 	                     <option value="200">200cm</option> 
									 	                     <option value="210">210cm</option> 
									 	                     <option value="220">220cm</option> 
									 	                </select>
									 	              </div>
										 	        </div>
										 	  		<div class="col-md-6 col-xs-12">
										 	  			<div class="form-group">
										 	  				<label for="">Speak english?</label>
									 	  					<div class="row">
										 	                    <div class="col-md-6">
										 	                      <select class="form-control" name="ingles" id="ingles">
										 	                        <option selected="" value="no">No</option>
										 	                        <option value="yes">Yes</option>
										 	                      </select>
										 	                    </div>
										 	                    <div class="col-md-6">
										 	                      <select class="form-control" name="nivel_ingles" id="nivel_ingles" disabled="">
										 	                        <option class="choose-ingles" disabled="" selected="">choose</option>
										 	                        <option value="low">Low</option>
										 	                        <option value="high">High</option>
										 	                      </select>
										 	                    </div>     
									 	                 	</div>
										 	  			</div>
										 	  		</div>
									 	            <div class="col-md-6 col-xs-12">
									 	              <div class="form-group">
									 	                 <label for="">Speak spanish?</label>
									 	                 <div class="row">
									 	                    <div class="col-md-6">
									 	                      <select class="form-control" name="espanol" id="espanol">
									 	                        <option selected="" value="no">No</option>
									 	                        <option value="yes">Yes</option>
									 	                      </select>
									 	                    </div>
									 	                    <div class="col-md-6">
									 	                      <select class="form-control" name="nivel_espanol" id="nivel_espanol" disabled="">
									 	                        <option disabled="" selected="">choose</option>
									 	                        <option value="low">Low</option>
									 	                        <option value="high">High</option>
									 	                      </select>
									 	                    </div>     
									 	                 </div>
									 	               </div>
									 	            </div>
									 	            <div class="col-md-6 col-xs-12">
									 	              <div class="form-group">
									 	                 <label for="">Speak french?</label>
									 	                 <div class="row">
									 	                    <div class="col-md-6">
									 	                      <select class="form-control" name="frances" id="frances">
									 	                        <option selected="" value="no">No</option>
									 	                        <option value="yes">Yes</option>
									 	                      </select>
									 	                    </div>
									 	                    <div class="col-md-6">
									 	                      <select class="form-control" name="nivel_frances" id="nivel_frances" disabled="">
									 	                        <option disabled="" selected="">choose</option>
									 	                        <option value="low">Low</option>
									 	                        <option value="high">High</option>
									 	                      </select>
									 	                    </div>     
									 	                 </div>
									 	               </div>
									 	            </div>
									 	            <div class="col-md-6 col-xs-12">
									 	              <div class="form-group">
									 	                 <label for="">Speak portuguese?</label>
									 	                 <div class="row">
									 	                    <div class="col-md-6">
									 	                      <select class="form-control" name="portugues" id="portugues">
									 	                        <option selected="" value="no">No</option>
									 	                        <option value="yes">Yes</option>
									 	                      </select>
									 	                    </div>
									 	                    <div class="col-md-6">
									 	                      <select class="form-control" name="nivel_portugues" id="nivel_portugues" disabled="">
									 	                        <option disabled="" selected="">choose</option>
									 	                        <option value="low">Low</option>
									 	                        <option value="high">High</option>
									 	                      </select>
									 	                    </div>     
									 	                 </div>
									 	               </div>
									 	            </div>
									 	            <div class="col-md-6 col-xs-12">
									 	              <div class="form-group">
									 	                 <label for="">Speak germany?</label>
									 	                 <div class="row">
									 	                    <div class="col-md-6">
									 	                      <select class="form-control" name="aleman" id="aleman">
									 	                        <option selected="" value="no">No</option>
									 	                        <option value="yes">Yes</option>
									 	                      </select>
									 	                    </div>
									 	                    <div class="col-md-6">
									 	                      <select class="form-control" name="nivel_aleman" id="nivel_aleman" disabled="">
									 	                        <option disabled="" selected="">choose</option>
									 	                        <option value="low">Low</option>
									 	                        <option value="high">High</option>
									 	                      </select>
									 	                    </div>     
									 	                 </div>
									 	               </div>
									 	            </div>
									 	  			<div class="col-md-6 col-xs-12">
									 	  				<div class="form-group">
									 	  					<div class="row">
									 	                  		<div class="col-md-6">
									 	                    		<label for="">Do you smoke?</label>
									 	                   			<select class="form-control" name="fumas" id="fumas">
											 	                      <option value="yes">Yes</option>  
											 	                      <option value="no">No</option>      
									 	                    		</select>
									 	                  		</div>
									 	                  		<div class="col-md-6">
									 	                   		  	<label for="">Do you drink?</label>
									 	                    		<select class="form-control"  name="tomas_licor" id="tomas_licor">
									 	                      			<option value="yes">Yes</option>  
									 	                      			<option value="no">No</option>      
									 	                    		</select>
									 	                  		</div>     
									 	                	</div>
									 	  				</div>
									 	  			</div>
									 	            <div class="col-md-12 col-xs-12">
									 	              	<div class="form-group">
									 	                  <label for="exampleFormControlFile1">Profile image</label>
									 	                  <input type="file" class="file" id="file" name="file">
									 	                </div>
									 	            </div>
										 	        {{-- </div> --}}
										 	  	</div>
										 		<button type="submit" class="btn btn-primary">Submit</button>
											</form>
										 </div> 			
									</div>
									<div class="card icard friends">
										<div class="card">
											<div class="card-header bg-primary text-white">
												FRIENDS
											</div>
											<div class="card-body">
												<h5>you do not have friends</h5>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					{{-- <div class="card-body">
						<div class="row">
							<div class="col-md-3">
								<div class="profile-img">
									@if(empty($data->image_url))
										@if($data->gender == 'male')
										<img class="img-fluid" src="https://www.w3schools.com/w3images/avatar2.png" alt="">
										@else
										<img class="img-fluid" src="https://www.w3schools.com/w3images/avatar6.png" alt="">
										@endif
									@else
										<img class="img-fluid" src="{{asset('img').'/'. $data->image_url}}" alt="">
									@endif
								</div>
							</div>
							<div class="col-md-9">
								<div class="row">
									<div class="col-md-12">
										<h3 class="">{{ ucwords($data->name) }} {{ ucwords($data->lastname) }}</h3>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label class="col-form-label"><strong>Email: </strong></label>
										<label class="col-form-label">{{ $data->email }}</label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<label class="col-form-label"><strong>Nationality: </strong></label>
										<label class="col-form-label">{{ ucwords($data->nationality) }}</label>
									</div>
									<div class="col-md-6">
										<label class="col-form-label"><strong>Country of residence: </strong></label>
										<label class="col-form-label">{{ ucwords($data->reside_country) }}</label>
									</div>
									<div class="col-md-6">
										<label class="col-form-label"><strong>Phone: </strong></label>
										<label class="col-form-label">{{ ucwords($data->cellphone) }}</label>
									</div>
									<div class="col-md-6">
										<label class="col-form-label"><strong>Membership: </strong></label>
										@if(empty($data->membership))
										<label class="col-form-label">without membership</label>
										@else
										<label class="col-form-label"> {{$data->membership}}</label>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div> --}}
					{{-- <form enctype="multipart/form-data" class="d-none" method="post" action="{{route('image_profile')}}">
					    {{ csrf_field() }}
					    <div class="form-group">
					        <input data-preview="#preview" name="input_img" type="file" id="imageInput">
					    </div>
					    <input type="submit" class="btn btn-primary" value="enviar">
					</form> --}}
				</div>
			</div>
		</div>
	</div>
</section>
	
@endsection