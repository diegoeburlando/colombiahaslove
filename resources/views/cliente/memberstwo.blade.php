@extends('layouts.app')

@section('content')
<div class="img-box">
	<img src="{{asset('img/image-banner.jpg')}}" alt="">		
</div>

<div class="container MAIN gallery-profiles">
	<div class="row">
		<div class="col-md-12">
			<div class="title-shadow-content">
				<div class="title-shadow">
					MEMBERS
				</div>
				<div class="title-shadow-text">
					<h3></h3><h3>MEMBERS</h3>
				</div>
			</div>
		</div>
		@foreach($person as $key=>$p)
		<div class="col-md-3 col-xs-12">
			<div class="card mb-2 item-profiles" id="{{$p->user_id}}">
				<img class="card-img-top" src="{{asset('img')}}/{{$p->profile_img}}">
				<div class="card-body">
					<h5 class="card-title">{{ucwords($p->name)}}</h5>
					<p class="profesion">{{$p->profesion}}&nbsp;</p>
					@if(Auth::user()->membership == 'premium' OR Auth::user()->membership == 'standar')
					<a href="{{route('get_profile',['id' => $p->id])}}" class="btn btn-primary btn-block">
					  see profile
					</a>
					@else
					<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#exampleModal">
					  see profile
					</button>
					@endif
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
<div class="modal fade modal-member" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">
        <h5 class="card-title text-center"><strong>Memberships</strong></h5>
        <div class="card-content">
        	  		<table class="table table-striped tabla-2">
        	  		  <thead>
        	  		    <tr class="cabezera-table2">
        	  		      <th scope="col"></th>
        	  		      <th class="title-standar" scope="col">
        	  				<div>
        	      				<img src="{{asset('img/standar.png')}}" alt="">
        	  				</div>
        	  		      </th>
        	  		      <th class="title-premium" scope="col">
        					<div>
        	    				<img src="{{asset('img/premium.png')}}" alt="">
        					</div>
        	  		      </th>
        	  		    </tr>
        	  		  </thead>
        	  		  <tbody>
        	  		    <tr class="ww">
        	  		      <td>Member 2 Member Direct Communications (via Skype, Telephone, Whatsapp, Email).</td>
        	  		      <td class="table-blue">3 per month</td>
        	  		      <td class="table-red">Unlimited</td>
        	  		    </tr>
        	  		    <tr>
        	  		      <td>Induvidual In Person Introductions</td>
        	  		      <td class="table-blue">4 per trimester</td>
        	  		      <td class="table-red">Unlimited</td>
        	  		    </tr>
        	  		    <tr class="ww">
        	  		      <td>In-Person Translation Assistance</td>
        	  		      <td class="table-blue"></td>
        	  		      <td class="table-red"><i class="fas fa-times"></i></td>
        	  		    </tr>
        	  		    <tr>
        	  		      <td>Access to Travel Assitance with our Alliance Agencia</td>
        	  		      <td class="table-blue"><i class="fas fa-times"></i></td>
        	  		      <td class="table-red"><i class="fas fa-times"></i></td>
        	  		    </tr>
        	  		    <tr class="ww">
        	  		      <td>Invitation to Group Activities in Colombia (paid members only)</td>
        	  		      <td class="table-blue"><i class="fas fa-times"></i></td>
        	  		      <td class="table-red"><i class="fas fa-times"></i></td>
        	  		    </tr>
        	  		    <tr>
        	  		      <td>Profile photos</td>
        	  		      <td class="table-blue">5</td>
        	  		      <td class="table-red">20</td>
        	  		    </tr>
        	  		    <tr class="ww">
        	  		      <td>Profile Videos</td>
        	  		      <td class="table-blue">1</td>
        	  		      <td class="table-red">3</td>
        	  		    </tr>
        	  		    <tr>
        	  		      <td>Search Filters</td>
        	  		      <td></td>
        	  		      <td class="table-red"><i class="fas fa-times"></i></td>
        	  		    </tr>
        	  		    <tr class="ww">
        	  		      <td>Hidden Profile Option</td>
        	  		      <td></td>
        	  		      <td class="table-red"><i class="fas fa-times"></i></td>
        	  		    </tr>
        	  		    <tr>
        	  		      <td>New Member Alerts</td>
        	  		      <td></td>
        	  		      <td class="table-red"><i class="fas fa-times"></i></td>
        	  		    </tr>
        	  		    <tr class="ww">
        	  		      <td></td>
        	  		      <td><a href="" class="membership-comprar">COMPRAR</a></td>
        	  		      <td><a href="" class="membership-comprar">COMPRAR</a></td>
        	  		    </tr>
        	  		  </tbody>
        	  		</table> 
        </div>
      </div>
  </div>
</div>

@endsection

