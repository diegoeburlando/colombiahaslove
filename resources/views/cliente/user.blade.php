@extends('layouts.app')

@section('content')

<div class="img-box h-200">
	<img src="{{asset('img/image-banner.jpg')}}" alt="">		
</div>
<div class="container">
	<div class="row">
		<div class="container profile-section" id="{{$data->user_id}}">
			<div class="row">
				<div class="col-md-5">
					<div class="row">
						<div class="col-md-3 col-xs-12">
							<?php $array = explode(',', $data->galeria);?>
							@foreach($array as $key => $item)
								<?php ($key == 0)? $active = "active": $active = ""; ?>
								<div class="item-profile-image {{$active}}">
									<img src="{{asset('/img')}}/{{$item}}" alt="" class="img-fluid">
								</div>
							@endforeach
						</div>
						<div class="col-md-9 col-xs-12">
							<div class="card card-profile shadow">
								<div class="item-profile-main-image">
									<img class="img-fluid" src="{{asset('/img')}}/{{$array[0]}}" alt="">
								</div>
								<div class="profile-nombre">
									<span>{{ucwords($data->name).' '.ucwords($data->lastname)}}</span>
								</div>								
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="profile-title"><h5>{{strtoupper($data->name). ' '. strtoupper($data->lastname)}}</h5></div>
					<div class="profile-profession"><p>{{ucwords($data->profesion)}}</p></div>
					<div class="profile-pestanas">
						<div class="row">
							<div class="col-md-3">
								<button class="btn btn-primary btn-sm btn-block tabs-button active" tabindex="profile">PROFILE</button>
							</div>
							<div class="col-md-3">
								<button class="btn btn-primary btn-sm btn-block tabs-button" tabindex="presentation">PRESENTATION</button>
							</div>
							<div class="col-md-3">
								<button class="btn btn-primary btn-sm btn-block tabs-button" tabindex="video">VIDEO</button>
							</div>
							<div class="col-md-3">
								<?php 
									$pendient = explode(',', $data->pendient);
									$friends = explode(',', $data->friends_id);
								?>
								@if(in_array(Auth::user()->id, $friends))
								<button class="btn btn-success btn-sm btn-block tabs-button" disabled tabindex="invitation"><i class="far fa-check-circle"></i> FRIENDS</button>
								@elseif(in_array(Auth::user()->id, $pendient) && !in_array(Auth::user()->id, $friends))
								<button class="btn btn-warning btn-sm btn-block tabs-button" disabled tabindex="invitation"><i class="far fa-clock"></i> WAITING</button>								
								@else
								<form method="post">
								@csrf
								<a class="btn btn-primary btn-sm btn-block tabs-button" id="addfriend" tabindex="invitation"><i class="fas fa-plus"></i> ADD FRIEND</a>
								<input type="hidden" id="current_id" value="{{Auth::user()->id}}">
								</form>
								@endif

							</div>
						</div>
					</div>
					<div class="profile-content active" id="profile">
						<div class="card card-content-profile">
							<div class="row">
								<div class="col-md-6">
										<div class="bag-box">
												<label for=""><strong>Nationality: </strong></label>
												<label>{{$data->nationality}}</label> 
										</div>
										<div class="bag-box">
											<label for=""><strong>I live in: </strong></label>
											<label>{{$data->ciudad}}</label> 
										</div>
										<hr>
										@if(in_array(Auth::user()->id, $friends))
										<h6>Datos de contacto</h6>
										<div class="bag-box">
											<label for=""><strong><i class="fab fa-whatsapp text-success"></i> Whatsapp : </strong></label>
											<label>{{$data->cellphone}}</label> 
										</div>
										<div class="bag-box">
											<label for=""><strong><i class="fas fa-envelope text-warning"></i> Email : </strong></label>
											<label>{{$data->email}}</label> 
										</div>
										<div class="bag-box">
											<label for=""><strong>Age: </strong></label>
											<label>{{$age}}</label> 
										</div>
										@endif

								</div>
								<div class="col-md-6">

										<div class="bag-box">
											<label for=""><strong>City: </strong></label>
											<label>{{$data->ciudad}}</label> 
										</div>
								</div>
							</div>							
						</div>
					</div>
					<div class="profile-content" id="presentation">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum impedit minus consectetur ad cupiditate accusamus fugiat esse ab eum. Nihil omnis accusantium tempora suscipit voluptas. Illum inventore aut laudantium quam.
						</p>
					</div>
					<div class="profile-content" id="video">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum impedit minus consectetur ad cupiditate accusamus fugiat esse ab eum. Nihil omnis accusantium tempora suscipit voluptas. Illum inventore aut laudantium quam.
						</p>
					</div>
					<div class="profile-content" id="invitation">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum impedit minus consectetur ad cupiditate accusamus fugiat esse ab eum. Nihil omnis accusantium tempora suscipit voluptas. Illum inventore aut laudantium quam.
						</p>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>

@endsection