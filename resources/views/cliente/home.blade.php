@extends('layouts.app')

@section('content')
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
 
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="http://colombiahaslove.com/wp-content/uploads/2019/01/banner1.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="//colombiahaslove.com/wp-content/uploads/2019/01/banner3.jpg" alt="Second slide">
    </div>
    {{-- <div class="carousel-item">
      <img class="d-block w-100" src="..." alt="Third slide">
    </div> --}}
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<section class="newsletter container-fluid">
	<div class="row">
		<div class="col-md-8 col-xs-12 mx-auto">
			<div class="card shadow-sm text-center card-newsletter">
				<h5 class="_primary">Hello</h5>
				<h2 class="title-newsletter">¡Meet Colombia Has Love!</h2>
				<div class="choose-gender">
					<div class="conter-1">
						<p>I AM:</p>
						<a href="">
							<img src="{{asset('img/icono3over.png')}}" alt="">
						</a>
						<a href="">
							<img src="{{asset('img/icono3over.png')}}" alt="">
						</a>
					</div>
					<div class="conter-2">
						<p>YOU WANT TO KNOW?</p>
					</div>
					<div class="conter-3">
						<a href="">
							<img src="{{asset('img/icono3over.png')}}" alt="">
						</a>
						<a href="">
							<img src="{{asset('img/icono3over.png')}}" alt="">
						</a>
						<a href="">
							<img src="{{asset('img/icono3over.png')}}" alt="">
						</a>
					</div>
				</div>
				<a class="btn btn-primary btn-see-profile ">SEE PROFILES →</a>
			</div>
		</div>
		<div class="col-md-8 col-xs-12 subtitle-newsletter mx-auto text-center">
			<h3><strong>Subscribe to our newsletter and we'll give you a free month</strong></h3>
			<div class="line-blue"></div>
		</div>
		<div class="col-md-6 col-xs-12 mx-auto">
			<form method="post" action="">
				<div class="form-newsletter">
					<input type="text" class="input-newsletter" placeholder="leave your mail">
					<button type="submit"><i class="fas fa-transgender"></i></button>
				</div>

			</form>
		</div>
	</div>
	
</section>
<section class="concept container-fluid">
	<div class="row">
		<div class="col-md-6 col-xs-12 concept-item">
			<div class="row align-items-center">
				<div class="title-shadow-content">
					<div class="title-shadow">
						OUR
					</div>
					<div class="title-shadow-text">
						<h3>OUR</h3><h3>CONCEPT</h3>
					</div>
				</div>
				<div class="title-mark">
					<h3>WE ARE ALEJANDRA Y MIGUEL</h3>
					<div class="blue-line"></div>
				</div>
			</div>
			<div class="content-concept">
				<div class="text-concept">
					<p>
						This new idea was born from an opportunity we saw developing over the last 10 years. As husband and wife, both in our second marriages, having lived combined totals of 50 years in both the USA and in Colombia, and having traveled to 20+ countries together, we realized that there was a unique opportunity in the international market place developing along with the increasing tourism growth in Colombia.
					</p>
				</div>
				<div class="button-concept">
					<a href="" class="btn btn-primary btn-see-profile">VIEW MORE →</a>
				</div>
				<img src="" alt="" class="comilla-left">
				<img src="" alt="" class="comilla-right">
			</div>
		</div>
		<div class="col-md-6 col-xs-12 concept-item">
			<div class="row align-items-center justify-content-center container-foto">
				<div class="div-blue">
					
				</div>
				<img src="https://colombiahaslove.com/wp-content/uploads/2019/02/ownconcept.jpg" alt="">
			</div>
		</div>
	</div>
</section>
<section class="about">
	<div class="row about-row-1">
		<div class="col-md-6 col-xs-12 uno">
			<div class="title-shadow-content">
				<div class="title-shadow">
					ABOUT
				</div>
				<div class="title-shadow-text">
					<h3>ABOUT</h3><h3>US</h3>
				</div>
			</div>
			<div class="container">
				<div class="item-about">
					<div class="item-img">
						<img class="img-fluid" src="{{asset('img/about1.png')}}" alt="">
					</div>
					<div class="item-content">
						<h4>We are Colombians, and know Colombia a country with a great beauty.</h4>
					</div>
				</div>
				<div class="item-about">
					<div class="item-img">
						<img class="img-fluid" src="{{asset('img/about2.png')}}" alt="">
					</div>
					<div class="item-content">
						<h4>We are extremely personalized and focused in our users to offer a great service.</h4>
					</div>
				</div>
				<div class="item-about">
					<div class="item-img">
						<img class="img-fluid" src="{{asset('img/about3.png')}}" alt="">
					</div>
					<div class="item-content">
						<h4>We are focused on developing relationships with serious, mature professionals, and that know who they are.</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="about-img">
				<img  src="{{ asset('img/somos.jpg') }}" alt="">
			</div>
		</div>
	</div>
	<div class="row about-row-2">
		<div class="col-md-6 col-xs-12">
			<div class="about-img">
				<img  src="{{ asset('img/nosomos.jpg') }}" alt="">
			</div>
		</div>
		<div class="col-md-6 col-xs-12 dos">
			<div class="title-shadow-content">
				<div class="title-shadow">
					WHO ARE
				</div>
				<div class="title-shadow-text">
					<h3>WHO ARE</h3><h3>WE NOT</h3>
				</div>
			</div>
			<div class="container dopy">
				<div class="item-about dos">
					<div class="item-content ">
						<h4>We do not support, or represent, or manage the idea of ​​an overnight adventure or short relationships.</h4>
					</div>
					<div class="item-img">
						<img class="img-fluid" src="{{asset('img/about4.png')}}" alt="">
					</div>
				</div>
				<div class="item-about dos">
					<div class="item-content">
						<h4>We do not promote or support illegal activities, directly or indirectly.</h4>
					</div>
					<div class="item-img">
						<img class="img-fluid" src="{{asset('img/about5.png')}}" alt="">
					</div>
				</div>
				<div class="item-about dos">
					<div class="item-content">
						<h4>We do not charge extra fees to be able to communicate directly from member to member.</h4>
					</div>
					<div class="item-img">
						<img class="img-fluid" src="{{asset('img/about6.png')}}" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="porque">
  <div class="porque-box d-flex hero-content-bet  mx-auto hero-align-item-center">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-4 col-xs-12">
  				    <div class="nuestro-one porque-item ">
  				        <div class="nuestro-one-img-box hero-relative" data-aos="fade-down" data-aos-duration="500">
  				          <a href="">
  				            <img class="img-fluid" src="{{asset('img/porque1.jpg')}}" alt="">
  				            <div class="nuestro-white-opacity hero-absolute"></div>
  				            <div class="nop-one">
  				              <div class="nop-one-one d-flex  mx-auto">
  				                <div class="nop-icon-one ">
  				                  <img clas="img-fluid" src="{{asset('img/iconoporque1.png')}}" alt="">
  				                </div>
  				                <div class="nop-icon-one-content  d-flex flex-direction-column">
  				                  <h3>WHY?</h3>
  				                  <h2>MEDELLIN</h2>
  				                </div>
  				              </div>
  				              <div class="nop-one-two">
  				                <p class="text-center text-white">
  				                		Medellin’s aesthetic, security, cost, easy living and real estate bargains clearly stood out to the scouts sent all over the world to create a report based on real-life experience of the locations.
  				                	</p>
  				                </p>
  				                <div class="nop-line"></div>
  				              </div>
  				              <div class="nop-one-three">
  				                <div class="nop-button-box">
  				                  <a href="{{Route('medellins')}}" class="btn-primary xq">KEEP WATCHING →</a>
  				                </div>
  				              </div>
  				            </div>
  				          </a>
  				        </div>
  				    </div>
  			</div>
  			<div class="col-md-4 col-xs-12">
  				    <div class="nuestro-one porque-item">
  				        <div class="nuestro-one-img-box hero-relative" data-aos="fade-down" data-aos-duration="1500">
  				          <a href="#">
  				            <img class="img-fluid" src="{{asset('img/porque2.jpg')}}" alt="">
  				            <div class="nuestro-white-opacity hero-absolute"></div>
  				            <div class="nop-one">
  				              <div class="nop-one-one d-flex mx-auto">
  				                <div class="nop-icon-one w-30">
  				                  <img clas="img-fluid" src="{{asset('img/iconoporque2.png')}}" alt="">
  				                </div>
  				                <div class="nop-icon-one-content w-70 d-flex flex-direction-column">
  				                  <h3>WHY?</h3>
  				                  <h2>COLOMBIA <br> HAS LOVE</h2>
  				                </div>
  				              </div>
  				              <div class="nop-one-two">
  				                <p class="text-center text-white">
  				                	We wanted to create a membership type group who have similar interests and enjoy the excitement and opportunity that this Group can provide.  We realized that it is challenging to arrive in an unknown country and try and meet people in a safe and interactive forum.
  				                </p>
  				                <div class="nop-line"></div>
  				              </div>
  				              <div class="nop-one-three">
  				                <div class="nop-button-box">
  				                  <a href="{{Route('hasloves')}}" class="btn-primary xq">KEEP WATCHING →</a>
  				                </div>
  				              </div>
  				            </div>
  				          </a>
  				        </div>
  				    </div>
  			</div>
  			<div class="col-md-4 col-xs-12">
  				    <div class="nuestro-one porque-item">
  				        <div class="nuestro-one-img-box hero-relative" data-aos="fade-down" data-aos-duration="2500">
  				          <a href="#">
  				            <img class="img-fluid" src="{{asset('img/porque3.jpg')}}" alt="">
  				            <div class="nuestro-white-opacity hero-absolute"></div>
  				              <div class="nop-one">
  				                <div class="nop-one-one d-flex mx-auto">
  				                  <div class="nop-icon-one w-30">
  				                    <img clas="img-fluid" src="{{asset('img/iconoporque3.png')}}" alt="">
  				                  </div>
  				                  <div class="nop-icon-one-content w-70 d-flex flex-direction-column">
  				                    <h3>WHY?</h3>
  				                    <h2>COLOMBIA</h2>
  				                  </div>
  				                </div>
  				                <div class="nop-one-two">
  				                  <p class="text-center text-white">
  				                  	The Colombians are exceptional. Colombia offers natural beauty and great tourism. Colombia has become a top country for retiring.
  				                  </p>
  				                  <div class="nop-line"></div>
  				                </div>
  				                <div class="nop-one-three">
  				                  <div class="nop-button-box">
  				                    <a href="{{Route('colombias')}}" class="btn-primary xq">KEEP WATCHING →</a>
  				                  </div>
  				                </div>
  				              </div>

  				            <div class="nop-three"></div>
  				          </a>
  				        </div>
  				    </div>
  			</div>
  		</div>
  	</div>
  </div>
</section>
@if(count($eventos) > 1)
<section class="eventos">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-5 col-xs-12 eventos-grid-1">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="title-shadow-content">
								<div class="title-shadow">
									EVENTS
								</div>
								<div class="title-shadow-text">
									<h3>OURS</h3><h3>EVENTS</h3>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 mt-5 eventos-subtitle">
							<h6>COLOMBIA HAS LOVE</h6>
							<div class="blue-line"></div>
							<p class="hola2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex voluptas nobis odio dicta, recusandae itaque, ipsum incidunt perspiciatis quo quas officia corporis voluptatibus earum iusto sit consectetur soluta dolores ratione.</p>
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-md-7 col-xs-12 eventos-grid-2">
				<div class="row row-eventos">
					<div class="div-blue"></div>
					<div class="row">
						<div class="col-md-8 mx-auto">
							<div class="row">

								@foreach($eventos as $event)
									<div class="col-md-6">
										<div class="card">
											<img class="card-img-top" src="{{images($event->img)}}" alt="">
											<div class="card-content">
												<h6 class="card-title">{{strtoupper($event->titulo)}}</h6>
												<p><strong>{{ucwords(cortar($event->titulo, 20))}}</strong></p>
												<a href="{{Route('single-event', ['id' => $event->id])}}" class="btn btn-primary btn-sm">SEE MORE</a>
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endif
<section class="travesia">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div class="title-shadow-content">
					<div class="title-shadow">
						TRAVESÍAS
					</div>
					<div class="title-shadow-text">
						<h3>TRAVESÍAS</h3><h3>Customized tourism</h3>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="travesia-box d-flex">
				  <div class="tcolum-one d-flex flex-direction-column w-33 img-i">
				    <div class="img-1 grand"><img class="img-i img-fluid" src="{{asset('img/tranvia.jpg')}}" alt=""></div>
				    <div class="img-2 grand"><img class="img-i img-fluid" src="{{asset('img/villadeleyva.jpg')}}" alt=""></div>

				  </div>
				  <div class="tcolum-two d-flex flex-direction-column w-66 img-i">
				    <div class="tcolum-two-1 d-flex w-100">
				      <div class="img-3  w-40 animated" data-aos="flip-left">
				        <img class="img-i img-fluid" src="{{asset('img/gris.jpg')}}" alt="">
				        <div class="tra-text">
				          <div class="nop-line"></div>
				          <img class="logo-travesia" src="{{asset('img/travesia-logo.jpg')}}" alt="">
				          <div class="nop-line"></div>
				        </div>
				      </div>

				      <div class="img-4 grand w-60"><img class="img-i img-fluid" src="{{asset('img/guatape.jpg')}}" alt=""></div>
				    </div>
				    <div class="tcolum-two-2 d-flex">
				      <div class="img-5 grand w-60"><img class="img-i img-fluid" src="{{asset('img/jardin.jpg')}}" alt=""></div>
				      <div class="img-6 grand w-40 animated" data-aos="flip-left">
				        <a href="https://www.colombianproperty.com/es" target="_blank">
				        <img class="img-i img-fluid" src="{{asset('img/colombianlogo.jpg')}}" alt="">
				        </a>
				      </div>
				    </div>
				    <div class="tcolum-two-3 d-flex">
				      <div class="img-7 w-60 animated" data-aos="flip-left">
				        <img class="img-i img-fluid" src="{{asset('img/gris.jpg')}}" alt="">
				        <div class="tra-text">
				          <div class="nop-line"></div>
				          <p class="text-center">IS ORIENTED TO THE ADVISING AND COMMERCIALIZATION</p>
				          <div class="nop-line"></div>
				          <div class="w-100 text-center"><a class="btn-theme-primary" href="http://www.travesiasturismo.com/" target="_blank">VIEW SITE</a></div>
				        </div>
				      </div>
				      <div class="img-8 grand w-40"><img class="img-i img-fluid" src="{{asset('img/bogota.jpg')}}" alt=""></div>
				    </div>
				  </div>
				</div>
			</div>
		</div>
	</div>
</section>
@if(count($testimonios) >= 6)
<section class="testimonios">
	<div class="container">
		<div class="col-md-12 col-xs-12">
			<div class="title-shadow-content">
				<div class="title-shadow">
					TESTIMONIES
				</div>
				<div class="title-shadow-text">
					<h3>THE</h3><h3>TESTIMONIES</h3>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 mt-5">
				<div class="row">
					<div class="col-md-12">
						<div class="row align-items-center justify-content-end testy">
							<div class="col-md-3">
								<img class="img-fluid" src="{{$testimonios[0]->img}}" alt="">
							</div>
							<div class="col-md-7">

								<p class="hola">Hello</p>
								<h5 class="_primary"><a href="{{Route('single-testimonie', ['id' => $testimonios[0]->id])}}">{{$testimonios[0]->titulo}}</a></h5>
								<p class="hola2">{{cortar($testimonios[0]->descripcion, 20)}}</p>
							</div>
						</div>
						<div class="row  align-items-center justify-content-center testy">
							<div class="col-md-3">
								<img class="img-fluid" src="{{$testimonios[1]->img}}" alt="">
							</div>
							<div class="col-md-7">
								<p class="hola">Hello</p>
								<h5 class="_primary"><a href="{{Route('single-testimonie', ['id' => $testimonios[1]->id])}}">{{$testimonios[1]->titulo}}</a></h5>
								<p class="hola2">{{cortar($testimonios[1]->descripcion, 20)}}</p>
							</div>
						</div>
						<div class="row align-items-center justify-content-end testy">
							<div class="col-md-3">
								<img class="img-fluid" src="{{$testimonios[2]->img}}" alt="">
							</div>
							<div class="col-md-7">
								<p class="hola">Hello</p>
								<h5 class="_primary"><a href="{{Route('single-testimonie', ['id' => $testimonios[2]->id])}}">{{$testimonios[2]->titulo}}</a></h5>
								<p class="hola2">{{cortar($testimonios[2]->descripcion, 20)}}</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-8 col-xs-12 mx-auto">
						<div class="testimonies-img">
							<img class="img-fluid" src="{{asset('img/parejatestimonios.png')}}" alt="">
						</div>				
					</div>
				</div>
			</div>
			<div class="col-md-4 mt-5">
				<div class="row">
					<div class="col-md-12">
						<div class="row align-items-center justify-content-start  testy">
							<div class="col-md-3">
								<img class="img-fluid" src="{{$testimonios[3]->img}}" alt="">
							</div>
							<div class="col-md-7">
								<p class="hola">Hello</p>
								<h5 class="_primary"><a href="{{Route('single-testimonie', ['id' => $testimonios[3]->id])}}">{{$testimonios[3]->titulo}}</a></h5>
								<p class="hola2">{{cortar($testimonios[3]->descripcion, 20)}}</p>
							</div>
						</div>
						<div class="row align-items-center justify-content-center  testy">
							<div class="col-md-3">
								<img class="img-fluid" src="{{$testimonios[4]->img}}" alt="">
							</div>
							<div class="col-md-7">
								<p class="hola">Hello</p>
								<h5 class="_primary"><a href="{{Route('single-testimonie', ['id' => $testimonios[4]->id])}}">{{$testimonios[4]->titulo}}</a></h5>
								<p class="hola2">{{cortar($testimonios[4]->descripcion, 20)}}</p>
							</div>
						</div>
						<div class="row align-items-center justify-content-start  testy">
							<div class="col-md-3">
								<img class="img-fluid" src="{{$testimonios[5]->img}}" alt="">
							</div>
							<div class="col-md-7">
								<p class="hola">Hello</p>
								<h5 class="_primary"><a href="{{Route('single-testimonie', ['id' => $testimonios[5]->id])}}">{{$testimonios[5]->titulo}}</a></h5>
								<p class="hola2">{{cortar($testimonios[5]->descripcion, 20)}}</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<a class="btn btn-primary" href="{{Route('testimonies')}}">SEE MORE</a>
			</div>
		</div>
		
	</div>
</section>
@endif
@endsection