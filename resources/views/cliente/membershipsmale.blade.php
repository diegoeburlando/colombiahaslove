@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="img-box">
		<img src="http://colombiahaslove.com/wp-content/uploads/2019/01/banner1.jpg" alt="">		
	</div>
</div>
<div class="container MAIN">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					Memberships
				</div>
				<div class="card-body">
					<table class="table table-striped">
					  <thead>
					    <tr>
					      <th scope="col"></th>
					      <th scope="col" class="text-success">Standar</th>
					      <th scope="col" class="text-danger">Premium</th>
					      <th scope="col" class="text-danger"></th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td><strong>Membership Options</strong></td>
					      <td><strong>Fees/Month: Total</strong></td>
					       <td><strong>Fees/Month: Total</strong></td>
					       <th scope="col" class="text-success">Choose</th>
					    </tr>
					    <tr>
					      <td>3 month</td>
					      <td>$75 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp $225</td>
					      <td>$95 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp $285</td>
					      <td><a class="btn btn-primary btn-sm" href="{{route('payment',['type' => 3])}}">Buy</a></td>
					    </tr>
					    <tr>
					      <td>6 month</td>
					      <td>$65 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp $390</td>
					      <td>$85 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp $510</td>
					      <td><a class="btn btn-primary btn-sm" href="{{route('payment',['type' => 6])}}">Buy</a></td>
					    </tr>
					    <tr>
					      <td>12 month</td>
					      <td>$55 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp $660</td>
					      <td>$75 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp $900</td>
					      <td><a class="btn btn-primary btn btn-sm" href="{{route('payment',['type' => 12])}}">Buy</a></td>
					    </tr>
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection