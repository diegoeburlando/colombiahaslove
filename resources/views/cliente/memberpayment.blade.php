@extends('layouts.app')

@section('content')

<div class="img-box">
	<img src="{{asset('img/image-banner.jpg')}}" alt="">		
</div>
<div class="container MAIN">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="title-shadow-content">
				<div class="title-shadow">
					MY
				</div>
				<div class="title-shadow-text">
					<h3>MY</h3><h3>MEMBERSHIP</h3>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					Status Memberships
				</div>
				<div class="card-body">
					<div class="">
						<label for=""><strong>Status Memberships: </strong> {{ucwords(Auth::user()->membership)}}</label>
					</div>
					<div class="">
						<label for=""><strong>Buyed: </strong> {{Auth::user()->start_membership}}</label>
					</div>

					<div class="">
						<label for=""><strong>Expired: </strong> {{Auth::user()->expired_membership}}</label>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

@endsection