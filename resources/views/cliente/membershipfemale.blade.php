@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="img-box">
		<img src="http://colombiahaslove.com/wp-content/uploads/2019/01/banner1.jpg" alt="">		
	</div>
</div>

<div class="container MAIN">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="title-shadow-content">
				<div class="title-shadow">
					OUR
				</div>
				<div class="title-shadow-text">
					<h3>OUR</h3><h3>MEMBERSHIPS</h3>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					Memberships
				</div>
				<div class="card-body">
					<table class="table table-striped">
					  <thead>
					    <tr>
					      <th scope="col"></th>
					      <th scope="col" class="text-success"></th>
					      <th scope="col" class="text-danger">Annual</th>
					      <th scope="col" class="text-danger"></th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td><strong>Membership Options</strong></td>
					      <td><strong></strong></td>
					       <td><strong>Fees/Month: Total</strong></td>
					       <th scope="col" class="text-success">Choose</th>
					    </tr>
					    <tr>
					      <td>12 month</td>
					      <td></td>
					      <td>$8.3 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp $100</td>
					      <td><a class="btn btn-primary btn-sm" href="href="{{route('payment',['type' => 1])}}">Buy</a></td>
					    </tr>
					  </tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>


@endsection