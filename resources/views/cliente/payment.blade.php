@extends('layouts.app')

@section('content')


<div class="container MAIN">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					Payment Gateway
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="row justify-content-center align-items-center">
								<div class="col-md-6">
									<div class="row justify-content-center align-items-center setpayment">
										<form action="" method="POST">
											<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
											<a class="ancle-paypal" id="ancle-paypal" index="{{Auth::user()->id}}" href="#">
												<img  class="img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/a/a4/Paypal_2014_logo.png" alt="">
											</a>
											
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row justify-content-center align-items-center">
								<div class="col-md-6">
									<div class="row justify-content-center align-items-center setpayment">
										<form action="" method="POST">
											<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
											<a class="ancle-payu" id="ancle-payu" index="{{Auth::user()->id}}" href="#">
												<img  class="img-fluid" src="https://www.payulatam.com/co/wp-content/uploads/sites/2/2017/06/logo.png" alt="">
											</a>
											
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</div>	
		</div>
	</div>
</div>


@endsection