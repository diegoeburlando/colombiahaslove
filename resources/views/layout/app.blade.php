<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{asset('/css/bootstrap/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('/css/responsive.css')}}">
	<title>Titulo</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg">
		<form method="POST" action="{{ route('logout') }}">
			{{ csrf_field() }}
			<button class="btn btn-danger">logout</button>
		</form>
	</nav>
	<div class="container">
		@yield('content')
	</div>
<footer>
	
	<script src="{{asset('js/jquery/jquery.min.js')}}"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/app.js')}}"></script>
	<script src="{{asset('js/index.js')}}"></script>
</footer>
</body>
</html>