@extends('layouts/admin')


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Id</th>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Country</th>
                  <th scope="col">Gender</th>
                  <th scope="col">Membership</th>
                </tr>
              </thead>
              <tbody>
                @foreach($users as $user)
                <tr>
                  <th scope="row">{{$user->user_id}}</th>
                  <td><a href="{{route('adminprofile', ['id' => $user->user_id])}}">{{$user->name}} {{$user->lastname}}</a></td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->pais}}</td>
                  <td>{{$user->gender}}</td>
                  <td>{{$user->membership}}</td>
                </tr>
                @endforeach

              </tbody>
            </table>
              {{ $users->links() }}
        </div>
    </div>
</div>

@endsection


