@extends('layouts/admin')

@section('content')

<div class="card">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2 col-xs-6">
                    <img class="img-fluid rounded-circle m-3" src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png" alt="">
                </div>
                <div class="col-md-10 p-5">
                    <h5>{{ucwords($user->name)}} {{ucwords($user->lastname)}}</h5>
                    <p>{{ucwords($user->profesion)}}</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection