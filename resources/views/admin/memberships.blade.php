@extends('layouts.admin')

@section('content')


<div class="row">
<div class="col-sm-12">
  <div class="full-right">
    <h2>All Posts</h2>
  </div>
</div>
</div>

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif
<div class="table-div">
	
<table class="table table-bordered">
  <tr>
    
    <th>Name</th>
    <th>3 month</th>
    <th>6 month</th>
    <th>12 month</th>
    <th>Member to member</th>
    <th>Person introduction</th>
    <th>Traslate</th>
    <th>Discounts Travesia</th>
    <th>Invitation to gruop</th>
    <th>Photos</th>
    <th>videos</th>
    <th>Filters</th>
    <th>Hide profile</th>
    <th>Alets new members</th>
  </tr>
  @foreach($membership as $member)
  	<form action="{{route('admineditmembership', $member->id)}}" class="form-memberships" id="{{$member->id}}" @method('POST')>
  		@csrf
  		<tr>
  		  <td>
  		  	{{$member->nombre}}
  		  	<input type="hide" id="id" name="id" value="{{$member->id}}" disabled>
  		  </td>
  		  <td>
  		  	<input type="text" id="meses_3" name="meses_3" value="{{$member->meses_3}}" disabled>
  		  </td>
  		  <td>
  		  	<input type="text" id="meses_6" name="meses_6" value="{{$member->meses_6}}" disabled>
  		  </td>
  		  <td>
  		  	<input type="text" id="meses_12" name="meses_12" value="{{$member->meses_12}}" disabled>
  		  </td>
  		  <td>
  		  	<input class="inputs-members" id="" name="" type="text" value="{{(intval($member->miembro_a_miembro))? 'YES': 'NO'}}" disabled>
  		  </td>
  		  
  		  <td>
  		  	<input class="inputs-members" id="" name="" type="text" value="{{(intval($member->acompanamiento))? 'YES': 'NO'}}" disabled>
  		  </td>
  		 
  		  <td>
  		  	<input class="inputs-members" id="" name="" type="text" value="{{(intval($member->traduccion))? 'YES': 'NO'}}" disabled>
  		  </td>
  		  
  		  <td>
  		  	<input class="inputs-members" id="" name="" type="text" value="{{(intval($member->acceso_travesia))? 'YES': 'NO'}}" disabled>
  		  </td>
  		 
  		  <td>
  		  	<input class="inputs-members" id="" name="" type="text" value="{{(intval($member->invitacion_grupo))? 'YES': 'NO'}}" disabled>
  		  </td>

  		  <td>
  		  	<input class="inputs-members" id="" name="" type="text" value="{{(intval($member->cantidad_foto))? '5': 'NO'}}" disabled>
  		  </td>

  		  <td>
  		  	<input class="inputs-members" id="" name="" type="text" value="{{(intval($member->video))? 'YES': 'NO'}}" disabled>
  		  </td>

  		  <td>
  		  	<input class="inputs-members" id="" name="" type="text" value="{{(intval($member->filtro_busqueda))? 'YES': 'NO'}}" disabled>
  		  </td>

  		  <td>
  		  	<input class="inputs-members" id="" name="" type="text" value="{{(intval($member->esconde_perfil))? 'YES': 'NO'}}" disabled>
  		  </td>
  		  

  		  <td>
  		  	<input class="inputs-members" id="" name="" type="text" value="{{(intval($member->alertas))? 'YES': 'NO'}}" disabled>
  		  </td>
  		  

  		  <td>
  		  	<input class="inputs-members" id="" name="" type="text" value="{{$member->tipo_usuario}}" disabled>
  		  </td>

  		  <td>
  		  	<a href="" class="edit-memberships"></a>
  		  </td>
  		  
  		 
  		</tr>
  	</form>
    
 @endforeach
</table>
</div>






@endsection