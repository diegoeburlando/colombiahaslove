@extends('layouts.app')

@section('content')
<div class="container mk-margin">
    <div class="row justify-content-center">
        <div class="col-md-12 col-xs-12">
            <div class="title-shadow-content">
                <div class="title-shadow">
                    REGISTER
                </div>
                <div class="title-shadow-text">
                    <h3>REGISTER</h3>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card pt-5">
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" autocomplete="off" autocomplete="off">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                               <div class="form-group row">
                                   <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                   <div class="col-md-8">
                                       <input id="name" type="text" tabindex="1" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                       @if ($errors->has('name'))
                                           <span class="invalid-feedback" role="alert">
                                               <strong>{{ $errors->first('name') }}</strong>
                                           </span>
                                       @endif
                                   </div>
                               </div>
                               <div class="form-group row">
                                   <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                                   <div class="col-md-8">
                                       <input id="email" type="email" tabindex="3" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                       @if ($errors->has('email'))
                                           <span class="invalid-feedback" role="alert">
                                               <strong>{{ $errors->first('email') }}</strong>
                                           </span>
                                       @endif
                                   </div>
                               </div>
                               <div class="form-group row">
                                   <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nationality') }}</label>
                                   <div class="col-md-8">
                                       <select id="nationality" tabindex="5" class="form-control{{ $errors->has('nationality') ? ' is-invalid' : '' }}" name="nationality" {{ old('nationality') }} required autofocus>
                                           <option disabled selected>choose</option>
                                           <option value="american">american</option>
                                           <option value="colombian">colombian</option>
                                       </select>
                                       @if ($errors->has('nationality'))
                                           <span class="invalid-feedback" role="alert">
                                               <strong>{{ $errors->first('nationality') }}</strong>
                                           </span>
                                       @endif
                                   </div>
                               </div>
                               <div class="form-group row">
                                   <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('I am') }}</label>
                                   <div class="col-md-8">
                                       <select name="gender" id="gender" class="form-control">
                                           <option disabled="" selected>choose</option>
                                           <option  value="female">female</option>
                                           <option  value="male">male</option>
                                           <option  value="both">both</option>
                                       </select>
                                        @if ($errors->has('gender'))
                                           <span class="invalid-feedback" role="alert">
                                               <strong>{{ $errors->first('gender') }}</strong>
                                           </span>
                                       @endif
                                   </div>
                               </div>
                               <div class="form-group row">
                                   <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                   <div class="col-md-8">
                                       <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                       @if ($errors->has('password'))
                                           <span class="invalid-feedback" role="alert">
                                               <strong>{{ $errors->first('password') }}</strong>
                                           </span>
                                       @endif
                                   </div>
                               </div>
                               <div class="form-group row">
                                   <label for="name" class="col-md-4 col-form-label text-md-right"></label>
                                   <div class="col-md-8">
                                       <div class="form-check form-check-inline">
                                         <input id="conditions" class="form-check-input ml-1" type="checkbox" name="conditions" id="inlineRadio1" value="female">
                                         <label class="form-check-label" for="inlineRadio1">I accept that Colombia has love send information to my email</label>
                                       </div>
                                        @if ($errors->has('conditions'))
                                           <span class="invalid-feedback" role="alert">
                                               <strong>{{ $errors->first('conditions') }}</strong>
                                           </span>
                                       @endif
                                   </div>
                                    <label for="password-confirm" class="col-md-4"></label>
                                   <div class="col-md-8">
                                       <button type="submit" class="btn btn-primary btn-block">
                                           {{ __('Register') }}
                                       </button>
                                   </div>
                                   
                               </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Lastname') }}</label>

                                    <div class="col-md-8">
                                        <input id="lastname" type="text" tabindex="2" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autofocus>

                                        @if ($errors->has('lastname'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('lastname') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Cellphone') }}</label>
                                    <div class="col-md-8">
                                        <input id="cellphone" type="text" tabindex="4" class="form-control{{ $errors->has('cellphone') ? ' is-invalid' : '' }}" name="cellphone" value="{{ old('cellphone') }}" required autofocus>

                                        @if ($errors->has('cellphone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('cellphone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Country of residence') }}</label>
                                    <div class="col-md-8">
                                        <select id="reside_country" tabindex="6" class="form-control{{ $errors->has('reside_country') ? ' is-invalid' : '' }}" name="reside_country" {{ old('reside_country') }} required autofocus>
                                            <option disabled selected>choose</option>
                                            <option value="usa">united state</option>
                                            <option value="colombia">colombia</option>
                                        </select>
                                        @if ($errors->has('reside_country'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('reside_country') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Looking for') }}</label>
                                    <div class="col-md-8">
                                        <select id="interesed" tabindex="" class="form-control{{ $errors->has('interesed') ? ' is-invalid' : '' }}" name="interesed" {{ old('reside_country') }} required autofocus>
                                            <option disabled selected>choose</option>
                                            <option value="female">female</option>
                                            <option value="male">male</option>
                                            <option value="both">both</option>
                                        </select>
                                         @if ($errors->has('interesed'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('interesed') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-8">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
