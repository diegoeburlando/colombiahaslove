$(document).ready(function(){

	$('.photo-galeria .card-img-top').on('click', function(){
		let name = $(this).attr('index');

		console.log(name)

		$('#'+name).click();

	})

	$('#fecha_nacimiento').datepicker({ changeYear:true, yearRange: "-100:+0"})


	$('.menu-profile .item-menu').on('click', function(){
		$('.menu-profile .item-menu').removeClass('active');
		$('.profile-element .icard').hide()
		$(this).addClass('active')

		let name = $(this).attr('id');

		$('.'+name).fadeIn()


	})

	$('.img-prev').on('click', function(){

		$('.carousel-control-prev').click();

	})

	$('.img-next').on('click', function(){

		$('.carousel-control-next').click();
		
		
	})
	


	$('#gender').on('change', function(){
		if($(this).val() == "male"){
			alert('male')
		}

		if($(this).val() == "female"){
			alert('female')
		}
	})


	$('.profile-img').on('click', function(){
		
		$('#imageInput').click()
		
	})


	function readURL(input) {

	  if (input.files && input.files[0]) {
	    var reader = new FileReader();

	    reader.onload = function(e) {
	      $('.profile-img img').attr('src', e.target.result);
	    }

	    reader.readAsDataURL(input.files[0]);
	  }
	}

	$("#imageInput").change(function() {
	  readURL(this);
	});


	$('#ancle-payu').on('click', function(e){
		e.preventDefault();
		let token = $('#token').val();
		
	})

	$('.setpayment a').on('click', function(e){
		e.preventDefault();

		$.ajaxSetup({
		  headers: {
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  }
		})

		$.ajax({
			url: "/api/payment/",
			type: 'POST',
			data: {
				id: $(this).attr('index'),
           		pay: $(this).attr('id'),
			},
			success: function(data) {

				console.log(data);
			   
				if(data.pay == 'ancle-payu'){

					swal({
					  title: "Thanks you!",
					  text: "We will send you a email confirmation when your membership has been activated",
					  icon: "success",
					});	


				}else if(data.pay == 'ancle-paypal'){
					swal({
					  title: "Thanks you!",
					  text: "We will send you a email confirmation when your membership has been activated",
					  icon: "success",
					}).then(()=>{

						window.location.href = "/";

					})
				}
			   
			}
         
       });

	})

	$('.item-profile-image').on('click', function(){

		$('.item-profile-image').removeClass('active')

		$(this).addClass('active');

		let src = $(this).find('img').attr('src');

		$('.item-profile-main-image img').attr('src', src).hide().fadeIn();

	})

	$('.tabs-button').on('click', function(){

		$('.tabs-button').removeClass('active');
		$('.tabs-button').removeClass('btn-danger');
		$(this).addClass('active')
		$(this).addClass('btn-danger')
		$('.profile-content').removeClass('active');

		let selector = $(this).attr('tabindex');
		$('#'+ selector).addClass('active').hide().fadeIn();

	})

	$('#addfriend').on('click', function(e){
		e.preventDefault();

		let id_user = $('.profile-section').attr('id');
		let token = $("input[name*='_token']").val();
		let id_session = $('#current_id').val();
		$.ajax({
		    url: '/api/v1/friend/',
		    headers : {'X-CSRF-TOKEN': token},
		    type : 'POST',
		    dataType : 'json',
		    data : {
		        id : id_user,
		        id_session : id_session
		        
		       	
		    },
		    success: function(res){

		        alert(res)
		        
		    }

		})
	})

})


