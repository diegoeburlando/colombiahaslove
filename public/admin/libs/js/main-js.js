
jQuery(document).ready(function($) {


    'use strict';

    $('#ingles').on('change', function(){
        if($(this).val() == 'yes'){
            $('#nivel_ingles').prop('disabled', false);
        }else if($(this).val() == 'no'){
            $('#nivel_ingles').val(null);
            $('#nivel_ingles').prop('disabled', true);
        }


    })

    $('#espanol').on('change', function(){
        if($(this).val() == 'yes'){
            $('#nivel_espanol').prop('disabled', false);
        }else if($(this).val() == 'no'){
             $('#nivel_espanol').val(null);
            $('#nivel_espanol').prop('disabled', true);
        }


    })

    $('#frances').on('change', function(){
        if($(this).val() == 'yes'){
            $('#nivel_frances').prop('disabled', false);
        }else if($(this).val() == 'no'){
             $('#nivel_frances').val(null);
            $('#nivel_frances').prop('disabled', true);
        }


    })

    $('#portugues').on('change', function(){
        if($(this).val() == 'yes'){
            $('#nivel_portugues').prop('disabled', false);
        }else if($(this).val() == 'no'){
             $('#nivel_portugues').val(null);
            $('#nivel_portugues').prop('disabled', true);
        }


    })

     $('#aleman').on('change', function(){
        if($(this).val() == 'yes'){
            $('#nivel_aleman').prop('disabled', false);
        }else if($(this).val() == 'no'){
             $('#nivel_aleman').val(null);
            $('#nivel_aleman').prop('disabled', true);
        }


    })



    $(document).on('click', '#alguna_vez_casado_si',function(){
        if($('.alguna_vez_casado_si').is(':checked')){
            $('#cuantas_veces').prop('disabled', false)
        }else{
            $('#cuantas_veces').prop('disabled', true)
        }
    })

    

    $(document).on('click', '#tiene_hijos',function(){
        if($('.tiene_hijos').is(':checked')){
            $('#cuantos_hijos').prop('disabled', false)
        }else{
            $('#cuantos_hijos').prop('disabled', true)
        }
    })
    $('#gender').on('change', function(){
        if($(this).val() == 1){

            let hombre = `<option disabled selected>choose</option><option value="sigle" class="">sigle</option><option value="married" class="">married</option>
            <option value="divorced" class="">divorced</option><option value="widower" class="">widower</option>`;
            $('#estado_civil').empty()
            $('#estado_civil').append(hombre)
            $('#estado_civil').prop('disabled', false)

        }

        if($(this).val() == 2){
            let mujer = `<option disabled selected>choose</option><option value="sigle" class="">sigle</option><option value="married" class="">married</option>
            <option value="divorced" class="">divorcee</option><option value="widower" class="">widow</option>`;
            $('#estado_civil').empty()
            $('#estado_civil').append(mujer)
            $('#estado_civil').prop('disabled', false)
        }
    })

    $('#fecha_nacimiento').datepicker({ changeYear:true, yearRange: "-100:+0"})

    // ============================================================== 
    // Notification list
    // ============================================================== 
    if ($(".notification-list").length) {

        $('.notification-list').slimScroll({
            height: '250px'
        });

    }

    // ============================================================== 
    // Menu Slim Scroll List
    // ============================================================== 


    if ($(".menu-list").length) {
        $('.menu-list').slimScroll({

        });
    }

    // ============================================================== 
    // Sidebar scrollnavigation 
    // ============================================================== 

    if ($(".sidebar-nav-fixed a").length) {
        $('.sidebar-nav-fixed a')
            // Remove links that don't actually link to anything

            .click(function(event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top - 90
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                };
                $('.sidebar-nav-fixed a').each(function() {
                    $(this).removeClass('active');
                })
                $(this).addClass('active');
            });

    }

    // ============================================================== 
    // tooltip
    // ============================================================== 
    if ($('[data-toggle="tooltip"]').length) {
            
            $('[data-toggle="tooltip"]').tooltip()

        }

     // ============================================================== 
    // popover
    // ============================================================== 
       if ($('[data-toggle="popover"]').length) {
            $('[data-toggle="popover"]').popover()

    }
     // ============================================================== 
    // Chat List Slim Scroll
    // ============================================================== 
        

        if ($('.chat-list').length) {
            $('.chat-list').slimScroll({
            color: 'false',
            width: '100%'


        });
    }
    // ============================================================== 
    // dropzone script
    // ============================================================== 

 //     if ($('.dz-clickable').length) {
 //            $(".dz-clickable").dropzone({ url: "/file/post" });
 // }

}); // AND OF JQUERY


// $(function() {
//     "use strict";


    

   // var monkeyList = new List('test-list', {
    //    valueNames: ['name']

     // });
  // var monkeyList = new List('test-list-2', {
    //    valueNames: ['name']

   // });



   
   

// });