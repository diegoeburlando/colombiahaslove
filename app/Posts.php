<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    //

    protected $fillable = ['titulo', 'img', 'galeria', 'video', 'descripcion', 'categoria'];
    protected $dates = ['created_at', 'updated_at'];
}
