<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'profile_img','password','nationality','reside_country', 'cellphone','gender', 'interesed', 'membership','start_membership', 'expired_membership', 'status'
    ];

    public function productos()
    {
     return $this->belongsToMany(Details::class);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function data_user($id){

        $user = DB::table('users as u')
        ->join('details as d', 'd.user_id', '=', 'u.id')
        ->select('u.id as user_id','u.name','u.lastname', 'u.profile_img','u.email','u.nationality','u.reside_country','u.cellphone','u.gender','u.interesed','u.membership','u.start_membership','u.expired_membership','u.friends_id','u.pendient','d.*')
        ->where('u.id', '=', $id)->get();

        
        // dd($user);
        return $user;

    }


    public function get_clientes(){

        $user = DB::table('users as u')
        ->join('details as d', 'd.user_id', '=', 'u.id')
        ->select('u.id as user_id','u.name','u.lastname', 'u.profile_img','u.email','u.nationality','u.reside_country','u.cellphone','u.gender','u.interesed','u.membership','u.start_membership','u.expired_membership','u.friends_id','u.pendient','d.*')
        ->where('u.user_type', '=', 1)->where('u.status', '=', 1)->orderBy('id', 'desc')->paginate(5);

        
        // dd($user);
        return $user;

    }
}
