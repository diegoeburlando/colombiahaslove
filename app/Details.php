<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Details extends Model
{


	protected $fillable = [
        'user_id',
        'fecha_nacimiento',
        'tipo_documento',
        'numero_document',
        'galeria',
        'direccion',
        'pais',
        'departamento',
        'ciudad',
        'barrio',
        'telefono',
        'celular',
        'alguna_vez_casado',
        'cuantas_veces',
        'estado_civil',
        'tiene_hijos',
        'cuantos_hijos',
        'quiere_hijos',
        'con_quien_vive',
        'puede_viajar',
        'visa_usa',
        'visa_europa',
        'visa_canada',
        'visa_otro',
        'paises_visitados',
        'actividades_hobbies',
        'profesion',
        'educacion',
        'como_te_enteraste',
        'ingles',
        'nivel_ingles',
        'espanol',
        'nivel_espanol',
        'frances',
        'nivel_frances',
        'portugues',
        'nivel_portugues',
        'aleman',
        'nivel_aleman',
        'fumas',
        'tomas_licor'
        
    ];
    

	 public function usuarios()
        {
         return $this->belongsToMany(User::class);
        }


}
