<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Memberships extends Model
{
    protected $fillable = [
    	'nombre','meses_3','meses_6','meses_12','miembro_a_miembro','acompanamiento','traduccion','acceso_travesia','invitacion_grupo','cantidad_foto','video','filtro_busqueda','esconde_perfil','alertas','tipo_usuario','status'

    ];
}
