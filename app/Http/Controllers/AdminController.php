<?php

namespace App\Http\Controllers;

use App\User;
use App\Details;
use App\Nationality;
use App\Category;
use App\Genders;
use App\Country;
use App\Memberships;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;


use Illuminate\Http\Request;

class AdminController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){

    	$user = new User;
    	$users = $user->get_clientes();

    	return view('admin.index', array(
    		'users' => $users
    	));
    }


    public function users(){
    	$user = new User;
    	$users = $user->get_clientes();

    	return view('admin.users', compact('users'));


    }

    public function details_user(Request $request){
    	$user = new User;
    	$id = $request->id;
    	$user = $user->data_user($id)[0];
    	
    	return view('admin.profile', compact('user'));
    }


    public function add_user(){
        $nacionalidad = new Nationality;

        $nacionalidad = $nacionalidad::All();

        $gender = new Genders;

        $gender = $gender::All();

        $pais = new Country;

        $p = $pais::All();
        
   


    	return view('admin.add', array(
                'nacionalidad' => $nacionalidad,
                'gender' => $gender,
                'p' => $p
            ));


    }

    public function store_user(Request $request){

         $user = User::create([
            'name' => $request->name,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'cellphone' => $request->cellphone,
            'nationality' => $request->nationality,
            'reside_country' => $request->reside_country,
            'gender' => $request->gender,
            'interesed' => $request->interesed,
            'password' => Hash::make('haslove123'),
            'status' => 1
        ]);

    	Details::create([
            'user_id' => $user->id,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'tipo_documento' => $request->tipo_documento,
            'numero_document' => $request->numero_document,
            'galeria' => $request->galeria,
            'direccion' => $request->direccion,
            'pais' => $request->pais,
            'departamento' => $request->departamento,
            'ciudad' => $request->ciudad,
            'barrio' => $request->barrio,
            'telefono' => $request->telefono,
            'celular' => $request->celular,
            'alguna_vez_casado' => $request->alguna_vez_casado,
            'cuantas_veces' => $request->cuantas_veces,
            'estado_civil' => $request->estado_civil,
            'tiene_hijos' => $request->tiene_hijos,
            'cuantos_hijos' => $request->cuantos_hijos,
            'quiere_hijos' => $request->quiere_hijos,
            'con_quien_vive' => $request->con_quien_vive,
            'puede_viajar' => $request->puede_viajar,
            'visa_usa' => $request->visa_usa,
            'visa_europa' => $request->visa_europa,
            'visa_canada' => $request->visa_canada,
            'visa_otro' => $request->visa_otro,
            'paises_visitados' => $request->paises_visitados,
            'actividades_hobbies' => $request->actividades_hobbies,
            'profesion' => $request->profesion,
            'educacion' => $request->educacion,
            'como_te_enteraste' => $request->como_te_enteraste,
            'ingles' => $request->ingles,
            'nivel_ingles' => $request->nivel_ingles,
            'espanol' => $request->espanol,
            'nivel_espanol' => $request->nivel_espanol,
            'frances' => $request->frances,
            'nivel_frances' => $request->nivel_frances,
            'portugues' => $request->portugues,
            'nivel_portugues' => $request->nivel_portugues,
            'aleman' => $request->aleman,
            'nivel_aleman' => $request->nivel_aleman,
            'fumas' => $request->fumas,
            'tomas_licor' => $request->tomas_licor,

        ]);

        

       if($request->file('file')){

            $path = Storage::disk('public')->put('image', $request->file('file'));
            $user->fill(['profile_img' => asset($path)])->save();
        }



        return redirect('administrador/usuarios');

    }


    public function memberships(){
        $membership = Memberships::All();

        return view('admin.memberships', array('membership' => $membership));

    }


    public function edit_membership(Request $request){

        $id = $request->id;

        return "hola";
    }

    
}
