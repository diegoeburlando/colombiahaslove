<?php

namespace App\Http\Controllers;
use App\Posts;
use App\Category;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Posts::orderBy('created_at', 'DESC')->paginate(8);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoria = Category::all();
        return view('posts.create', compact('categoria'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
                 'titulo'=>'required|string|max:255',
                 'descripcion'=>'required',
                 'categoria'=>'required'

               ]);

    // $post->tags()->sync($request->get);


       $post = Posts::create($request->all());

       if($request->file('file')){

            $path = Storage::disk('public')->put('image', $request->file('file'));
            $post->fill(['img' => asset($path)])->save();
        }



       return redirect()->route('posts.index')->with('success','Post created success');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Posts::find($id);
        return view('posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias = Category::all();

        $post = Posts::find($id);
        return view('posts.edit',compact('post', 'categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        
        $this->validate($request,[
          'titulo' => 'required',
          'descripcion' => 'required'

        ]);
        Posts::find($id)->update($request->all());
        return redirect()->route('posts.index')->with('success','Post update success');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
           Posts::find($id)->delete();
  
            return redirect()->route('posts.index')
                        ->with('success','Product deleted successfully');
    
    }

    public function testimonios(){

        $testimonios = DB::table('posts')->where('categoria', '=', 'testimonios')->inRandomOrder()->get();
        // dd($testimonios);
        return view('posts.alltestimonies', compact('testimonios'));
    }

     public function single_testimonio(Request $request){

        $testimonio = DB::table('posts')->find($request->id);
        
        return view('posts.singletestimonie', compact('testimonio'));
    }

    public function eventos(){

        $testimonios = DB::table('posts')->where('categoria', '=', 'eventos')->orderBy('created_at', 'ASC')->get();
        // dd($testimonios);
        return view('posts.alleventos', compact('eventos'));
    }

     public function single_evento(Request $request){

        $evento = DB::table('posts')->find($request->id);
        
        return view('posts.singleevent', compact('evento'));
    }

    public function medellins(){

        $medellins = DB::table('posts')->where('categoria', '=', 'medellin')->orderBy('created_at', 'ASC')->get();
        
        return view('posts.allmedellins', compact('medellins'));
    }

     public function single_medellin(Request $request){

        $medellin = DB::table('posts')->find($request->id);
        
        return view('posts.singlemedellin', compact('medellin'));
    }

    public function colombias(){

        $colombias = DB::table('posts')->where('categoria', '=', 'colombia')->orderBy('created_at', 'ASC')->get();
        
        return view('posts.allcolombias', compact('colombias'));
    }

     public function single_colombia(Request $request){

        $colombia = DB::table('posts')->find($request->id);
        
        return view('posts.singlecolombia', compact('colombia'));
    }


    public function hasloves(){

        $hasloves = DB::table('posts')->where('categoria', '=', 'haslove')->orderBy('created_at', 'ASC')->get();
        
        return view('posts.allhasloves', compact('hasloves'));
    }

     public function single_haslove(Request $request){

        $haslove = DB::table('posts')->find($request->id);
        
        return view('posts.singlehaslove', compact('haslove'));
    }
}
