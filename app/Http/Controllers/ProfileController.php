<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Details;
use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{

    public function store($request){

    	return $request->all();


    }


    public function memberships(){

    	if(Auth::check()){

            if(Auth::user()->membership == 'premium'){


            return view('cliente.memberpayment');   


        }else{
            if(Auth::user()->gender == "female"){

                return view('cliente.membershipfemale');


            }else{

                return view('cliente.membershipsmale');

            }
        }   
        

    		
    		

    		
    	}else{

    		return redirect('login');
    		
    	}

    	

    }

    public function members(){

         $person = "";
            

        if(Auth::check()){
            $person = DB::table('users')
                ->join('details', 'details.user_id', '=', 'users.id')
                ->where('users.gender', '=',Auth::user()->interesed)->where('users.user_type', '=', 1)->inRandomOrder()->get();
                // dd($person);

            return view('cliente.memberstwo', compact('person'));



        }else{
            $person = DB::table('users')
                ->join('details', 'details.user_id', '=', 'users.id')
                ->where('users.user_type', '=', 1)->whereIn('users.id', [6,7,8])->inRandomOrder()->get();
                // dd($person);

            return view('cliente.membersone', compact('person'));

        }

    }

    public function get_profile(Request $request){
        $user = new User();
        $id = $request->id;

        $data = $user->data_user($id)[0];
        $age = Carbon::parse($data->fecha_nacimiento)->age;
        return view('cliente.user', array(
            'data' => $data,
            'age' => $age

        ));


    }


    public function userfoto(Request $request){

        if($request->file('foto0')){

            $path = Storage::disk('public')->put('image', $request->file('foto0'));
            $user = User::find($request->user_id);

           $user->profile_img = $path;

           $user->save();
            
           return redirect('profile');
            
        }

        if($request->file('foto1')){

            $path = Storage::disk('public')->put('image', $request->file('foto1'));
            $details = Details::find($request->id);

           $details->foto1 = $path;

           $details->save();
            
           return redirect('profile');
            
        }

        if($request->file('foto2')){

            $path = Storage::disk('public')->put('image', $request->file('foto2'));
            $details = Details::find($request->id);

            $details->foto2 = $path;

            $details->save();
            return redirect('profile');
            
        }

        if($request->file('foto3')){

            $path = Storage::disk('public')->put('image', $request->file('foto3'));
            $details = Details::find($request->id);

            $details->foto3 = $path;

            $details->save();
             return redirect('profile');   
        }

        if($request->file('foto4')){

            $path = Storage::disk('public')->put('image', $request->file('foto4'));
            $details = Details::find($request->id);

            $details->foto4 = $path;

            $details->save();
            
            return redirect('profile');
            
        }
        

    }
}
