<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Posts;
use DB;
use App\Details;
use App\Nationality;
use App\Category;
use App\Genders;
use App\Country;
use App\Memberships;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $testimonios = DB::table('posts')->where('categoria', '=', 'testimonios')->orderBy('created_at')->take(8)->get();
        $eventos = DB::table('posts')->where('categoria', '=', 'eventos')->orderBy('created_at', 'ASC')->take(2)->get();
        return view('cliente.home', array('testimonios' => $testimonios, 'eventos' => $eventos));
    }

    public function profile(){

        $nacionalidad = new Nationality;

        $nacionalidad = $nacionalidad::All();

        $gender = new Genders;

        $gender = $gender::All();

        $pais = new Country;

        $p = $pais::All();

        $id = Auth::user()->id;
        $user = new User();
        $object = $user->data_user($id);
        $data = $object[0];
        // dd($data);
        return view('cliente.profile', array('data' => $data, 'nacionalidad' => $nacionalidad, 'gender' => $gender, 'p' => $p));
    }


  
}
